<?php

namespace App\Models;

use Exception;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Training extends Model
{
    use HasFactory;

    public static function storeTrainingDetails($post){
        try{
            $TrainingDetailsArray=[                
                'userid' => auth()->user()->id,
                'personalid' => auth()->user()->personalid,
                'trainingproviderinstitutionalname' => $post[ 'trainingproviderinstitutionalname'],
                'trainingname' => $post[ 'trainingname'],
                'gradedivisionpercent' => $post['gradedivisionpercent'],
                'fromdatebs' => $post[ 'fromdatebs'],
                'enddatebs' => $post[ 'enddatebs' ],
                'fromdatead' => $post['fromdatead'],
                'enddatead' => $post[ 'enddatead'],
                'document' => !empty($post['documents']) ?$post['documents'] : $post['back_document'],
                'postedby' => auth()->user()->id,
                'posteddatetime' => date('Y-m-d H:i:s'),  
    
            ];

            DB::beginTransaction();
            if(empty($post['trainingdetailid'])){
                $result=DB::table('trainings')->insert($TrainingDetailsArray);
            }else{
            $result=DB::table('trainings')->where('id',$post['trainingdetailid'])->update($TrainingDetailsArray); 
            }
            DB::commit();
            return true;
        }catch(Exception $e){
            DB::rollback();
            throw $e;
        }
}

public static function getTrainingDetailsData($post)
    {
        $cond = "t.status='Y'";
        $limit = 15;
        $offset = 0;
        $get = $_GET;
        foreach ($get as $key => $value) {
            $get[$key] = trim(strtolower(htmlspecialchars($get[$key], ENT_QUOTES)));
        }
        if (!empty($_GET["iDisplayLength"])) {
            $limit = $_GET['iDisplayLength'];
            $offset = $_GET["iDisplayStart"];
        }


        if ($get['sSearch_1'])
            $cond .= " AND lower(trainingproviderinstitutionalname) like '%" . $get['sSearch_1'] . "%'";

            $sql = "Select  (select count(*) from trainings where status='Y') as totalrecs,t.* from trainings as t
            where " . $cond . " and t.postedby= " . $post['userid'] . " order by t.id desc";

        if ($limit > -1) {
            $sql = $sql . ' limit ' . $limit . ' offset ' . $offset . '';
        }
        $result = DB::select($sql);
        if ($result) {
            $ndata = $result;
            $ndata['totalrecs'] = @$result[0]->totalrecs ? $result[0]->totalrecs : 0;
            $ndata['totalfilteredrecs'] = @$result[0]->totalrecs ? $result[0]->totalrecs : 0;
        } else {
            $ndata = array();
        }
        return $ndata;
    }

    public static function previousAllData($post){
        try{
            $result=DB::table('trainings')->where('id',$post['trainingdetailid'])->first();
            return $result;
        }catch(Exception $e){
            throw $e;
        }
    }

    public static function deleteTrainingDetailsData($post)
    {
        try {
            DB::beginTransaction();
            DB::table('trainings')->where('id', $post['trainingdetailid'])->update(['status' => 'R']);
            DB::commit();
            return true;
        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }
}
