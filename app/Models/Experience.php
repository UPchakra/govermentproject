<?php

namespace App\Models;

use Exception;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Experience extends Model
{
    use HasFactory;

    public static function storeExperienceDetails($post){
        try{

            $experinceDetailsArray=[
                'userid' => auth()->user()->id,
                'personalid' => auth()->user()->personalid,
                'officename' => $post[ 'officename'],
                'officeaddress' => $post['officeaddress'],
                'designation' => $post[ 'designation'],
                'jobtype' => $post[ 'jobtype'],
                'service' => $post[ 'service' ],
                'group' => $post['group'],
                'subgroup' => $post[ 'subgroup'],
                'ranklabel' => $post['ranklabel'],
                'fromdatebs' => $post['fromdatebs'],
                'enddatebs' => $post[ 'enddatebs'],
                'workingstatus' => $post['workingstatus'],
                'workingstatuslabel' => $post['workingstatuslabel'],
                'document' => !empty($post['documents']) ?$post['documents'] : $post['back_document'],
                'postedby' => auth()->user()->id,
                'posteddatetime' => date('Y-m-d H:i:s'),  

            ];
            DB::beginTransaction();
            if(empty($post['experiencedetailid'])){
                $result=DB::table('experiences')->insert($experinceDetailsArray);
            }else{
            $result=DB::table('experiences')->where('id',$post['experiencedetailid'])->update($experinceDetailsArray);
                
            }
            DB::commit();
            return true;

        }catch(Exception $e){
            DB::rollback();
            throw $e;
        }
    }

    public static function getExperienceDetailsData($post)
    {
        $cond = "e.status='Y'";
        $limit = 15;
        $offset = 0;
        $get = $_GET;
        foreach ($get as $key => $value) {
            $get[$key] = trim(strtolower(htmlspecialchars($get[$key], ENT_QUOTES)));
        }
        if (!empty($_GET["iDisplayLength"])) {
            $limit = $_GET['iDisplayLength'];
            $offset = $_GET["iDisplayStart"];
        }


        if ($get['sSearch_1'])
            $cond .= " AND lower(municipalityname) like '%" . $get['sSearch_1'] . "%'";

            $sql = "Select  (select count(*) from experiences where status='Y') as totalrecs,e.* from experiences as e
            where " . $cond . " and e.postedby= " . $post['userid'] . " order by e.id desc";


        if ($limit > -1) {
            $sql = $sql . ' limit ' . $limit . ' offset ' . $offset . '';
        }
        $result = DB::select($sql);
        if ($result) {
            $ndata = $result;
            $ndata['totalrecs'] = @$result[0]->totalrecs ? $result[0]->totalrecs : 0;
            $ndata['totalfilteredrecs'] = @$result[0]->totalrecs ? $result[0]->totalrecs : 0;
        } else {
            $ndata = array();
        }
        return $ndata;
    }

    public static function previousAllData($post){
        try{
            $result=DB::table('experiences')->where('id',$post['experiencedetailid'])->first();
            return $result;
        }catch(Exception $e){
            throw $e;
        }
    }

    public static function deleteExperienceDetailsData($post)
    {
        try {
            DB::beginTransaction();
            DB::table('experiences')->where('id', $post['experiencedetailid'])->update(['status' => 'R']);
            DB::commit();
            return true;
        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }
}
