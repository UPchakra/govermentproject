<?php

namespace App\Models;

use Exception;
use Faker\Provider\ar_EG\Person;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Personal extends Model
{
    use HasFactory;
    protected $timestamp = false;

    public static function storePersonalDetails($post)
    {
        try {
            $personalDetailsArray = [
                'userid' => auth()->user()->id,
                'nfirstname' => $post['nfirstname'],
                'nmiddlename' => $post['nmiddlename'],
                'nlastname' => $post['nlastname'],
                'efirstname' => $post['efirstname'],
                'emiddlename' => $post['emiddlename'],
                'elastname' => $post['elastname'],
                'dateofbirthbs' => $post['dateofbirthbs'],
                'dateofbirthad' => $post['dateofbirthad'],
                'gender' => $post['gender'],
                'fatherfirstname' => $post['fatherfirstname'],
                'fathermiddlename' => $post['fathermiddlename'],
                'fatherlastname' => $post['fatherlastname'],
                'motherfirstname' => $post['motherfirstname'],
                'mothermiddlename' => $post['mothermiddlename'],
                'motherlastname' => $post['motherlastname'],
                'grandfatherfirstname' => $post['grandfatherfirstname'],
                'grandfathermiddlename' => $post['grandfathermiddlename'],
                'grandfatherlastname' => $post['grandfatherlastname'],
                'citizenshipnumber' => $post['citizenshipnumber'],
                'citizenshipissuedistrictid' => $post['citizenshipissuedistrictid'],
                'citizenshipissuedate' => $post['citizenshipissuedate'],
                'postedby' => auth()->user()->id,
                'posteddatetime' => date('Y-m-d H:i:s'),
            ];
            DB::beginTransaction();
            $personalid=$post['personaldetailid'];

            if (empty($post['personaldetailid'])) {
                $result = DB::table('personals')->insertGetId($personalDetailsArray);
                $personalid=$result;
            } else {
                $result = DB::table('personals')->where('id', $post['personaldetailid'])->update($personalDetailsArray);
            }
            $response=[
                "success" => true,
                "id" => $personalid
            ];
            DB::table('users')->where('id', auth()->user()->id)->update([
                'personalid'=>$personalid
            ]);
            session()->put('personalid',$personalid);

            DB::commit();

            
            
            
        } catch (Exception $e) {
            DB::rollback();
            $response=[
                "success" => true,
                "id" => $e->getMessage()
            ];
        }
        return $response;
    }

    public function documents()
    {
    }
    public function otherdetails()
    {
    }
    public function contactdetails()
    {
    }
    public function qulifications()
    {
    }

    public static function previousAllData($post){
        $result=DB::table('personals')->where('id',$post['personaldetailid'])->first();
        return $result;
    }
}