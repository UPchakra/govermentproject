<?php

namespace App\Models;

use Exception;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Contactdetail extends Model
{
    use HasFactory;

    public static function storeContactDetails($post){
        try{
            $contactDetailsArray=[                
                'userid' => auth()->user()->id,
                'personalid' => auth()->user()->personalid,
                'provinceid' => $post['provinceid'],
                'districtid' => $post['districtid'],
                'municipalityid' => $post[ 'municipalityid'],
                'ward' => $post['ward' ],
                'tole' => $post['tole'],
                'marga' => $post['marga'],
                'housenumber' => $post['housenumber'],
                'tempoprovinceid' => $post['tempoprovinceid'],
                'tempodistrictid' => $post['tempodistrictid'],
                'tempomunicipalityid' => $post['tempomunicipalityid'],
                'tempoward' => $post['tempoward' ],
                'tempotole' => $post['tempotole'],
                'tempomarga' => $post['tempomarga'],
                'tempohousenumber' => $post['tempohousenumber'],
                'tempophonenumber' => $post['tempophonenumber'],
                'maillingaddress' => $post['maillingaddress'],
                'postedby' => auth()->user()->id,
                'posteddatetime' => date('Y-m-d H:i:s'),  

            ];
            DB::beginTransaction();
            if(empty($post['contactdetailid'])){
                $result=DB::table('contactdetails')->insert($contactDetailsArray);
            }else{
            $result=DB::table('contactdetails')->where('id',$post['contactdetailid'])->update($contactDetailsArray);
                
            }
            DB::commit();
            $response=[
                "success" => true,
                "id" => $post['personalid']
            ];

        }catch(Exception $e){
            DB::rollback();
            $response=[
                "success" => false,
                "id" => $e->getMessage()
            ];
        }
        return $response;

    }
}
