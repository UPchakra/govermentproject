<?php

namespace App\Models;

use Exception;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Education extends Model
{
    use HasFactory;
    public static function storeEducationDetails($post){
        try{

            $educationDetailsArray=[
                'userid' => auth()->user()->id,
                'personalid' => auth()->user()->personalid,
                'universityboardname' => $post[ 'universityboardname'],
                'educationlevel' => $post['educationlevel'],
                'educationfaculty'=>$post['educationfaculty'],
                'educationinstitution' => $post[ 'educationinstitution'],
                'devisiongradepercentage' => $post[ 'devisiongradepercentage' ],
                'mejorsubject' => $post['mejorsubject'],
                'qulificationawardeddetails' => $post[ 'qulificationawardeddetails'],
                'passoutdatead' => $post['passoutdatead'],
                'passoutdatebs' => $post[ 'passoutdatebs'],
                'educationaltype' => $post['educationaltype'],
                'academicdocument' => !empty($post[ 'academicdocument']) ? $post[ 'academicdocument'] : $post['back_academicdocument'],
                'equivalentdocument' => !empty($post[ 'equivalentdocuments']) ? $post[ 'equivalentdocuments'] : $post['back_equivalentdocument'],
                'postedby' => auth()->user()->id,
                'posteddatetime' => date('Y-m-d H:i:s'),        
            ];
            DB::beginTransaction();
            if(empty($post['educationdetailid'])){
                $result=DB::table('educations')->insert($educationDetailsArray);
            }else{
            $result=DB::table('educations')->where('id',$post['educationdetailid'])->update($educationDetailsArray);
                
            }
            DB::commit();
            return true;

        }catch(Exception $e){
            DB::rollback();
            throw $e;
        }
    }

    public static function getEducationDetailsData($post)
    {
        $cond = "e.status='Y'";
        $limit = 15;
        $offset = 0;
        $get = $_GET;
        foreach ($get as $key => $value) {
            $get[$key] = trim(strtolower(htmlspecialchars($get[$key], ENT_QUOTES)));
        }
        if (!empty($_GET["iDisplayLength"])) {
            $limit = $_GET['iDisplayLength'];
            $offset = $_GET["iDisplayStart"];
        }


        if ($get['sSearch_1'])
            $cond .= " AND lower(e.universityboardname) like '%" . $get['sSearch_1'] . "%'";
            if ($get['sSearch_1'])
            $cond .= " AND lower(e.educationlevel) like '%" . $get['sSearch_1'] . "%'";

       
            $sql = "Select  (select count(*) from educations where status='Y') as totalrecs,e.*,a.name from educations as e
            join academics as a on a.id=e.educationlevel
           where " . $cond . " and e.postedby= " . $post['userid'] . " order by e.id desc";
            
        if ($limit > -1) {
            $sql = $sql . ' limit ' . $limit . ' offset ' . $offset . '';
        }
        $result = DB::select($sql);
        if ($result) {
            $ndata = $result;
            $ndata['totalrecs'] = @$result[0]->totalrecs ? $result[0]->totalrecs : 0;
            $ndata['totalfilteredrecs'] = @$result[0]->totalrecs ? $result[0]->totalrecs : 0;
        } else {
            $ndata = array();
        }
        return $ndata;
    }

    public static function previousAllData($post){
        try{
            $result=DB::table('educations')->where('id',$post['educationdetailid'])->first();
            return $result;
        }catch(Exception $e){
            throw $e;
        }
    }

    public static function deleteEducationDetailsData($post)
    {
        try {
            DB::beginTransaction();
            DB::table('educations')->where('id', $post['educationdetailid'])->update(['status' => 'R']);
            DB::commit();
            return true;
        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }
}
