<?php

namespace App\Models;

use Exception;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Extradetail extends Model
{
    use HasFactory;

    public static function storeExtraDetails($post){
        try{
            
            $extraDetailsArray=[                
                'userid' => auth()->user()->id,
                'personalid' => auth()->user()->personalid,
                'cast' => $post[ 'cast'],
                'religion' => $post[ 'religion'],
                'religionother' => $post['religionother'],
                'maritalstatus' => $post[ 'maritalstatus'],
                'employmentstatus' => $post[ 'employmentstatus' ],
                'employmetothers' => $post['employmetothers'],
                'motherlanguage' => $post[ 'motherlanguage'],
                'disabilitystatus' => $post['disabilitystatus'],
                'disabilityoverview' => $post[ 'disabilityoverview'],
                // 'fatherandmotheroccupation' => $post['fatherandmotheroccupation'],
                // 'fatherandmotheroccupationother' => $post['fatherandmotheroccupationother'],
                // 'whatyouwantoyourself' => $post['whatyouwantoyourself'],
                // 'whatyouwantoyourselfother' => $post['whatyouwantoyourselfother'],
                // 'castgroup' => $post['castgroup'],
                // 'other' => $post['other'],
                'postedby' => auth()->user()->id,
                'posteddatetime' => date('Y-m-d H:i:s'),  
            ];
            DB::beginTransaction();
            if(empty($post['extradetailid']) || $post['extradetailid']=='null' || $post['extradetailid']==null){
                
                $result=DB::table('extradetails')->insert($extraDetailsArray);
            }else{
                $result=DB::table('extradetails')->where('id',$post['extradetailid'])->update($extraDetailsArray); 
            }
            DB::commit();
            $response=[
                "success" => true,
                "id" => $post['personalid']
            ];

        }catch(Exception $e){
            DB::rollback();
            $response=[
                "success" => false,
                "id" => $e->getMessage()
            ];

        }
        return $response;
    }
}
