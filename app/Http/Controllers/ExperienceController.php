<?php

namespace App\Http\Controllers;

use App\Models\Experience;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Image;
use Validator;
class ExperienceController extends Controller
{ 
    //
    public function index()
    {
        return view('admin.pages.experience.viewexperiencesetup');
    }

   /**
    * A function that is used to add school setup data.
    *
    * @param Request request The request object.
    */
    public function experienceForm(Request $request)
    {
        $post = $request->all();

        $personalDetails=DB::table('personals')->select('id')->where('userid',auth()->user()->id)->first();
        if(!empty(@$post['experiencedetailid'])){
            $previousData=Experience::previousAllData($post);
                }
        $data=[
            'previousData'=>@$previousData,
            'saveurl'=>route('storeExperienceDetails'),
            'personalid'=>@$personalDetails->id
        ];


        return view('admin.pages.experience.addexperiencesetup',$data);
    }

    public function storeExperienceDetails(Request $request){
        try{
            $post=$request->all();
            $rules = [
                'officename' => 'required',
                'officeaddress' => 'required',
                'jobtype' => 'required',
                'designation' => 'required',
                'service' => 'required',
                'group' => 'required',
                'subgroup' => 'required',
                'ranklabel' => 'required',
                'fromdatebs' => 'required',
                'enddatebs' => 'required',
                'workingstatus' => 'required',
            ];

            $messages = [
                'officename.required' => 'तपाईले कार्यालयको नाम भर्नुभएको छैन |',
                'officeaddress.required' => 'तपाईले कार्यालय ठेगाना भर्नुभएको छैन |',
                'jobtype.required' => 'तपाईले कामको प्रकार भर्नुभएको छैन |',
                'designation.required' => 'तपाईले पद भर्नुभएको छैन |',
                'service' => 'तपाईले सेवा छैन |',
                'group.required' => 'तपाईले समूह भर्नुभएको छैन |',
                'subgroup.required' => 'तपाईले उपसमूह भर्नुभएको छैन |',
                'ranklabel.required' => 'तपाईले श्रेणी स्तर भर्नुभएको छैन |',
                'fromdatebs.required' => 'तपाईले सुरू मिति (नेपालीमा) भर्नुभएको छैन |',
                'enddatebs.required' => 'तपाईले अन्त्य मिति (नेपालीमा) भर्नुभएको छैन |',
                'workingstatus.required' => 'तपाईले काम गर्ने स्थिति भर्नुभएको छैन |',
            ];

            $validator = Validator::make($request->all(), $rules, $messages);
            if ($validator->fails()) {
                throw new Exception($validator->errors()->first(), 1);
            }

            $type="success";
            $message="Experience Details has been saved Successfully.";
            $post['documents']=[];
            if ($request->hasFile('document')) {
                $image_tmp = $request->file('document');
                foreach ($image_tmp as $id=>$file) {
                if ($file->isValid()) {
                    $extension = $file->getClientOriginalExtension();

                    $originalFilename = explode('.',$file->getClientOriginalName())[0];
                    $current = date('Ymd');
                    $filename = $originalFilename.'-'.$current.'-' . rand(111, 99999) . '.' . $extension;
                    $image_path = public_path('uploads/experience/' . $filename);
                    $pdf_path = public_path('uploads/experience/');

                    if($extension=="pdf"||$extension=="pdf"){
                        $file->move($pdf_path, $filename);

                    }else{
                        Image::make($file)->save($image_path);
                    }
                    $post['documents'][$id] = $filename;
                }
            }
            $post['documents'] = json_encode($post['documents']);
            }

            $response=Experience::storeExperienceDetails($post);

        }catch(Exception $e){
            $response=false;
            $type='errro';
            $message=$e->getMessage();

        }
        echo json_encode(array('type'=>$type,'message'=>$message,'response'=>$response));

    }

    public function getExperienceDetailsData(Request $request)
    {
        $post = $request->all();
        $post['userid']=auth()->user()->id;
        $data = Experience::getExperienceDetailsData($post);
        $i = 0;
        $array = array();
        $filtereddata = (@$data["totalfilteredrecs"] > 0 ? $data["totalfilteredrecs"] : @$data["totalrecs"]);
        $totalrecs = @$data["totalrecs"];

        unset($data["totalfilteredrecs"]);
        unset($data["totalrecs"]);
        foreach ($data as $row) {
            $array[$i]["sn"] = $i + 1 ;
            $array[$i]["officename"] = $row->officename;
            $array[$i]["officeaddress"] = $row->officeaddress;
            $array[$i]["jobtype"] = $row->jobtype;
            $array[$i]["designation"] = $row->designation;
            $array[$i]["ranklabel"] = $row->ranklabel;
            $array[$i]["workingstatuslabel"] = $row->workingstatuslabel;
            $array[$i]["workingstatus"] = $row->workingstatus;
            $array[$i]["fromdatebs"] = $row->fromdatebs;
            $array[$i]["enddatebs"] = $row->enddatebs;
            $images=json_decode(@$row->document);
            $urls='';
            if(!empty($images)){
            foreach (@$images as $image){
                $urls .= '<a style="padding-left:10px;" title="'.@$image.'" href="'.asset('uploads/experience/' . @$image).'" download><i class="fa fa-download"></i></a>';
            }}
            $array[$i]["document"] =$urls;
            $action = "";
            // if($this->isedit){
            $action .= '<a href="javascript:;" class="editExperienceDetail" data-experiencedetail="' . $row->id . '"  data-user="' . $row->userid . '"  data-persional="'.$row->personalid.'"><i class="fa fa-edit">EDIT</i></a>';
            // }
            // if($this->isdelete){
            $action .= '&nbsp | &nbsp <a href="javascript:;" class="deleteExperienceDetail"  data-experiencedetail="' . $row->id . '" data-user="' . $row->userid . '"  data-persional="'.$row->personalid.'"><i class="fa fa-trash">DELETE</i></a>';
            // }
            $array[$i]["action"] = $action;
            $i++;
        }
        if (!$filtereddata) {
            $filtereddata = 0;
        }
        if (!$totalrecs) {
            $totalrecs = 0;
        }

        echo json_encode(array("recordsFiltered" => @$filtereddata, "recordsTotal" => @$totalrecs, "data" => $array));
        exit;

    }
    public function deleteExperienceDetailsData(Request $request)
    {
        try {
            $type = 'success';
            $message = 'Experience Details has been Deeted Successfully.';
            $post = $request->all();
            $response = Experience::deleteExperienceDetailsData($post);
        } catch (Exception $e) {
            $type = 'error';
            $message = $e->getMessage();
            $response = false;
        }
        echo json_encode(['type' => $type, 'message' => $message]);
    }
}
