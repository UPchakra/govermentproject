<?php

namespace App\Http\Controllers;

use App\Models\Vacancy;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Image;
use Validator;
class VacancyController extends Controller
{
    public function index()
    {
        return view('admin.vacancy.viewvacancysetup');
    }

   /**
    * A function that is used to add school setup data.
    *
    * @param Request request The request object.
    */
    public function vacancyForm(Request $request)
    {
        $post = $request->all();
        $levels= DB::table('levels')->where('status','Y')->get();
        $servicegroups= DB::table('servicegroups')->where('status','Y')->get();
        $jobcategories= DB::table('jobcategories')->where('status','Y')->get();
        $designations= DB::table('designations')->where('status','Y')->get();
        $academics = DB::table('academics')->where('status','Y')->get();


        if(!empty(@$post['vacancysetupid'])){
            $previousData=Vacancy::previousAllData($post);
            }

        $data=[
            'previousData'=>@$previousData,
            'saveurl'=>route('storeVacancyDetails'),
            'levels' => $levels,
            'servicegroups' => $servicegroups,
            'jobcategories' => $jobcategories,
            'designations' => $designations,
            'academics' => $academics,

            ];
        return view('admin.vacancy.addvacancysetup',$data);
    }

    public function storeVacancyDetails(Request $request){
        try{
            $post=$request->all();
            $rules = [
                'vacancynumber' => 'required',
                'level' => 'required',
                'designation' => 'required',
                'servicesgroup'=>'required',
                'jobcategory'=>'required',
                'academicid' => 'required',
                'vacancyrate'=>'required',
                'numberofvacancy'=>'required',
                'vacancypublishdate'=>'required',
                'vacancyenddate'=>'required',
                'extendeddate'=>'required',
                'jobstatus'=>'required',
            ];

            $messages = [
                'vacancynumber.required' => 'तपाईले रिक्त स्थान नम्बर भर्नुभएको छैन |',
                'level.required' => 'तपाईले स्तर भर्नुभएको छैन |',
                'designation.required' => 'तपाईले पदनाम भर्नुभएको छैन |',
                'servicesgroup.required' => 'तपाईले सेवा समूह भर्नुभएको छैन |',
                'jobcategory.required' => 'तपाईले काम को श्रेणी भर्नुभएको छैन |',
                'academicid.required' => 'तपाईले योग्यता भर्नुभएको छैन |',
                'vacancyrate.required' => 'तपाईले रिक्तता दर भर्नुभएको छैन |',
                'numberofvacancy.required' => 'तपाईले रिक्तता संख्या भर्नुभएको छैन |',
                'vacancypublishdate.required' => 'तपाईले रिक्त पद प्रकाशित मिति भर्नुभएको छैन |',
                'vacancyenddate.required' => 'तपाईले रिक्तता समाप्ति मिति भर्नुभएको छैन |',
                'extendeddate.required' => 'तपाईले विस्तारित मिति भर्नुभएको छैन |',
                'jobstatus.required' => 'तपाईले कामको स्थिति भर्नुभएको छैन |',
            ];
            $validator = Validator::make($request->all(), $rules, $messages);
            if ($validator->fails()) {
                throw new Exception($validator->errors()->first(), 1);
            }

            $type="success";
            $message="Vacancy Setup has been Saved Successfully.";

            $response=Vacancy::storeVacancyDetails($post);

        }catch(Exception $e){
            $response=false;
            $type='errro';
            $message=$e->getMessage();

        }
        echo json_encode(array('type'=>$type,'message'=>$message,'response'=>$response));

    }

    public function getVacancyDetailsData(Request $request)
    {
        $post = $request->all();
        $post['userid']=auth()->user()->id;
        $data = Vacancy::getVacancyDetailsData($post);
        $i = 0;
        $array = array();
        $filtereddata = (@$data["totalfilteredrecs"] > 0 ? $data["totalfilteredrecs"] : @$data["totalrecs"]);
        $totalrecs = @$data["totalrecs"];

        unset($data["totalfilteredrecs"]);
        unset($data["totalrecs"]);
        foreach ($data as $row) {
            $array[$i]["sn"] = $i + 1 ;
            $array[$i]["vacancynumber"] = $row->vacancynumber;
            $array[$i]["level"] = $row->labelname;
            $array[$i]["designation"] = $row->title;
            $array[$i]["servicesgroup"] = $row->servicegroupname;
            $array[$i]["jobcategory"] = $row->name;
            $array[$i]["vacancyrate"] = $row->vacancyrate;
            $array[$i]["numberofvacancy"] = $row->numberofvacancy;
            $array[$i]["vacancypublishdate"] = $row->vacancypublishdate;
            $array[$i]["vacancyenddate"] = $row->vacancyenddate;
            $array[$i]["extendeddate"] = $row->extendeddate;
            $array[$i]["jobstatus"] = $row->jobstatus;
            $array[$i]["qualification"] = $row->qualification;

            $action = "";
            // if($this->isedit){
            $action .= '<a href="javascript:;" class="editVacancySetup" data-vacancysetup="' . $row->id . '"  ><i class="fa fa-edit">EDIT</i></a>';
            // }
            // if($this->isdelete){
            $action .= '&nbsp | &nbsp <a href="javascript:;" class="deleteVacancySetup"  data-vacancysetup="' . $row->id . '" ><i class="fa fa-trash">DELETE</i></a>';
            // }
            $array[$i]["action"] = $action;
            $i++;
        }
        if (!$filtereddata) {
            $filtereddata = 0;
        }
        if (!$totalrecs) {
            $totalrecs = 0;
        }

        echo json_encode(array("recordsFiltered" => @$filtereddata, "recordsTotal" => @$totalrecs, "data" => $array));
        exit;

    }
    public function deleteVacancyDetailsData(Request $request)
    {
        try {
            $type = 'success';
            $message = 'Vancacy Setup has been Deeted Successfully.';
            $post = $request->all();
            $response = Vacancy::deleteVacancyDetailsData($post);
        } catch (Exception $e) {
            $type = 'error';
            $message = $e->getMessage();
            $response = false;
        }
        echo json_encode(['type' => $type, 'message' => $message]);
    }
}
