<?php

namespace App\Http\Controllers;

use App\Models\Contactdetail;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;

class ContactDetailsController extends Controller
{
    //
    public function contactdetailForm(Request $request)
    {   
        $post= $request->all();
        $provinces=DB::table('provinces')->where('status','Y')->get();
        $districts=DB::table('districts')->where('status','Y')->get();

        $previousData=[];
        if(!empty(@$post['personaldetailid'])){

            $tabledata=DB::table('contactdetails')->where('personalid',$post['personaldetailid'])->first();
            if($tabledata){
                $previousData=$tabledata;
            }
        }
        $data=[
            'provinces'=>$provinces,
            'districts'=>$districts,
            'previousData'=>@$previousData,
        ];
        return view('admin.pages.contactdetails.contactdetails',$data);
    }


    public function storeContactDetails(Request $request){
        try{
            $post=$request->all();

            $rules = [
                'provinceid' => 'required',
                'districtid' => 'required',
                'municipalityid' => 'required',
                'ward' => 'required',
                'tole' => 'required',
            ];

            $messages = [
                'provinceid.required' => 'तपाईले प्रदेश भर्नुभएको छैन |',
                'districtid.required' => 'तपाईले जिल्ला भर्नुभएको छैन |',
                'municipalityid.required' => 'तपाईले पालिकाको नाम  भर्नुभएको छैन |',
                'ward.required' => 'तपाईले वार्ड भर्नुभएको छैन |',
                'tole.required' => 'तपाईले टोल भर्नुभएको छैन |',
            ];

            $validator = Validator::make($request->all(), $rules, $messages);
            if ($validator->fails()) {
                throw new Exception($validator->errors()->first(), 1);
            }
            $type="success";
            $message="Contact Details has been saved Successfully.";

            $response=Contactdetail::storeContactDetails($post);

            $response=[
                "redirectUrl"=>"education",
                "success"=>true,
                "message"=>$message,
            ];
        }catch(Exception $e){
            $message=$e->getMessage();
            $response=[
                "redirectUrl"=>"otherdetailForm",
                "success"=>false,
                "message"=>$message,
            ];

        }
        echo json_encode(array($response));
    }
}
