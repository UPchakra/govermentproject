<?php

namespace App\Http\Controllers;

use App\Models\Payment;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PaymentController extends Controller
{
    public function paymentKhalti()
    {
        return view('admin.payment.index');
    }

    public function VerifyKhaltiPayment(Request $request)
    {
 
        $post = $request->all();

        $args = http_build_query(array(
            'token' => $post['token'],
            'amount' => $post['amount'],
        ));

        $url = "https://khalti.com/api/v2/payment/verify/";

        # Make the call using API.
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $args);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $keys = config('app.khalti_secret_key');
        $headers = ['Authorization: Key ' . $keys . ''];
        // $headers = ['Authorization: Key test_secret_key_97db055019bb457a93a27a4b60cddd60'];
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        // Response
        $response = curl_exec($ch);
        $status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        return  $post;

    }

    public function StorePayment(Request $request)
    {

        try {
            $post = $request->all();
            $type = 'success';
            $message = "Payment Succeffully.";
            DB::beginTransaction();
            $result = Payment::StorePaymentDetails($post);

            if (!$result) {
                throw new Exception("Error Processing Request", 1);
            }
            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
            $type = 'error';
            $message = $e->getMessage();
            $response = null;
        }

        echo json_encode(array('type' => $type, 'message' => $message));
    }

    
}
