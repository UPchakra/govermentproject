<?php

namespace App\Http\Controllers;

use App\Models\Training;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Image;
use Validator;

class TrainingController extends Controller
{
    //
    public function index()
    {
        return view('admin.pages.training.viewtrainingsetup');
    }

    /**
     * A function that is used to add school setup data.
     *
     * @param Request request The request object.
     */
    public function trainingForm(Request $request)
    {
        $post = $request->all();
        $personalDetails = DB::table('personals')->select('id')->where('userid', auth()->user()->id)->first();
        if (!empty(@$post['trainingdetailid'])) {
            $previousData = Training::previousAllData($post);
        }
        $data = [
            'previousData' => @$previousData,
            'saveurl' => route('storeTrainingDetails'),
            'personalid' => @$personalDetails->id
        ];


        return view('admin.pages.training.addtrainingsetup', $data);
    }

    public function storeTrainingDetails(Request $request)
    {
        try {
            $post = $request->all();
            $rules = [
                'trainingproviderinstitutionalname' => 'required',
                'trainingname' => 'required',
                'fromdatebs' => 'required',
                'enddatebs' => 'required',
                'fromdatead' => 'required',
                'enddatead' => 'required',
            ];

            $messages = [
                'trainingproviderinstitutionalname.required' => 'तपाईले प्रशिक्षण प्रदायक संस्थाको नाम भर्नुभएको छैन |',
                'trainingname.required' => 'तपाईले तालिमको नाम भर्नुभएको छैन |',
                'fromdatebs.required' => 'तपाईले सुरू मिति (नेपालीमा) भर्नुभएको छैन |',
                'enddatebs.required' => 'तपाईले अन्त्य मिति (नेपालीमा) भर्नुभएको छैन |',
                'fromdatead.required' => 'तपाईले सुरू मिति (अंग्रेजीमा) भर्नुभएको छैन |',
                'enddatead.required' => 'तपाईले सुरू मिति (अंग्रेजीमा) भर्नुभएको छैन |',
            ];

            $validator = Validator::make($request->all(), $rules, $messages);
            if ($validator->fails()) {
                throw new Exception($validator->errors()->first(), 1);
            }
            $type = "success";
            $message = "Training Details has been saved Successfully.";
            $post['documents']=[];
            if ($request->hasFile('document')) {
                $image_tmp = $request->file('document');
                foreach ($image_tmp as $id => $file) {
                    if ($file->isValid()) {
                        $extension = $file->getClientOriginalExtension();
                        $originalFilename = explode('.', $file->getClientOriginalName())[0];
                        $current = date('Ymd');
                        $filename = $originalFilename . '-' . $current . '-' . rand(111, 99999) . '.' . $extension;
                        $image_path = public_path('uploads/training/' . $filename);
                        $pdf_path = public_path('uploads/training/');


                        if ($extension == "pdf" || $extension == "pdf") {
                            $file->move($pdf_path, $filename);
                        } else {
                            Image::make($file)->save($image_path);
                        }
                        $post['documents'][$id] = $filename;
                    }
                }
            }
            $post['documents'] = json_encode($post['documents']);
            $response = Training::storeTrainingDetails($post);
        } catch (Exception $e) {
            $response = false;
            $type = 'errro';
            $message = $e->getMessage();
        }
        echo json_encode(array('type' => $type, 'message' => $message, 'response' => $response));
    }

    public function getTrainingDetailsData(Request $request)
    {
        $post = $request->all();
        $post['userid'] = auth()->user()->id;
        $data = Training::getTrainingDetailsData($post);
        $i = 0;
        $array = array();
        $filtereddata = (@$data["totalfilteredrecs"] > 0 ? $data["totalfilteredrecs"] : @$data["totalrecs"]);
        $totalrecs = @$data["totalrecs"];

        unset($data["totalfilteredrecs"]);
        unset($data["totalrecs"]);
        foreach ($data as $row) {
            $array[$i]["sn"] = $i + 1;
            $array[$i]["trainingproviderinstitutionalname"] = $row->trainingproviderinstitutionalname;
            $array[$i]["trainingname"] = $row->trainingname;
            $array[$i]["gradedivisionpercent"] = $row->gradedivisionpercent;
            $array[$i]["fromdatead"] = $row->fromdatead;
            $array[$i]["enddatead"] = $row->enddatead;
            $images=json_decode(@$row->document);
            $urls='';
            if(!empty($images)){
            foreach ($images as $image){
                $urls .= '<a style="padding-left:10px;" title="'.@$image.'" href="'.asset('uploads/training/' . @$image).'" download><i class="fa fa-download"></i></a>';
            }
        }
            $array[$i]["document"] =$urls;
            $action = "";
            // if($this->isedit){
            $action .= '<a href="javascript:;" class="editTrainingDetail" data-trainingdetail="' . $row->id . '"  data-user="' . $row->userid . '"  data-persional="' . $row->personalid . '"><i class="fa fa-edit">EDIT</i></a>';
            // }
            // if($this->isdelete){
            $action .= '&nbsp | &nbsp <a href="javascript:;" class="deleteTrainingDetail"  data-trainingdetail="' . $row->id . '" data-user="' . $row->userid . '"  data-persional="' . $row->personalid . '"><i class="fa fa-trash">DELETE</i></a>';
            // }
            $array[$i]["action"] = $action;
            $i++;
        }
        if (!$filtereddata) {
            $filtereddata = 0;
        }
        if (!$totalrecs) {
            $totalrecs = 0;
        }

        echo json_encode(array("recordsFiltered" => @$filtereddata, "recordsTotal" => @$totalrecs, "data" => $array));
        exit;
    }

    public function deleteTrainingDetailsData(Request $request)
    {
        try {
            $type = 'success';
            $message = 'Training Details has been Deeted Successfully.';
            $post = $request->all();
            $response = Training::deleteTrainingDetailsData($post);
        } catch (Exception $e) {
            $type = 'error';
            $message = $e->getMessage();
            $response = false;
        }
        echo json_encode(['type' => $type, 'message' => $message]);
    }
}
