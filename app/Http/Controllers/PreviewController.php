<?php

namespace App\Http\Controllers;

use App\Models\Vacancy;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\DB;

class PreviewController extends Controller
{
    //
    public function preview()
    {
        $userid = auth()->user()->personalid;
        // dd($userid);
        $profile = DB::table('personals')->where(['userid'=>auth()->user()->id])->first();
        $extraDetails = DB::table('extradetails')->where(['userid'=>auth()->user()->id])->first();
        // $contactDetails = DB::table('contactdetails')->where(['userid'=>2])->first();

        $contactSql = "SELECT
                * 
            FROM
                (
                SELECT
                    Ad.*,
                    P.provincename,
                    D.districtname,
                    M.vdcormunicipalitiename 
                FROM
                    (
                    SELECT
                        userid,
                        provinceid,
                        districtid,
                        municipalityid,
                        ward,
                        tole,
                        marga,
                        housenumber,
                        phonenumber 
                    FROM
                        contactdetails 
                    WHERE
                        userid = 2 
                        AND STATUS = 'Y' 
                    ) AS AD
                    JOIN provinces AS P ON P.id = AD.provinceid
                    JOIN districts AS D ON D.id = AD.districtid
                    JOIN vdcormunicipalities AS M ON M.id = AD.municipalityid 
                ) AS PERM
                JOIN (
                SELECT
                    Ad.*,
                    P.provincename AS tempprovincename,
                    D.districtname AS tempdistrictname,
                    M.vdcormunicipalitiename AS tempvdcormunicipalitiename 
                FROM
                    (
                    SELECT
                        userid AS id,
                        tempoprovinceid,
                        tempodistrictid,
                        tempomunicipalityid,
                        tempoward,
                        tempotole,
                        tempomarga,
                        tempohousenumber,
                        tempophonenumber,
                        maillingaddress 
                    FROM
                        contactdetails 
                    WHERE
                        userid = 2 
                        AND STATUS = 'Y' 
                    ) AS AD
                    JOIN provinces AS P ON P.id = AD.tempoprovinceid
                    JOIN districts AS D ON D.id = AD.tempodistrictid
                JOIN vdcormunicipalities AS M ON M.id = AD.tempomunicipalityid 
                ) AS TEMP ON TEMP.id = PERM.userid ";

        $contactDetails = DB::select($contactSql);

        $educationDetails = DB::table('educations as e')
            ->selectRaw('universityboardname,educationfaculty,educationinstitution,devisiongradepercentage,mejorsubject,name')
            ->join('academics as a','a.id', '=', 'e.educationlevel')
            ->where(['userid'=>2])->get()->all();

        $trainingDetails = DB::table('trainings')
            ->selectRaw('trainingname, trainingproviderinstitutionalname, gradedivisionpercent, fromdatebs, fromdatead, fromdatebs, enddatebs, enddatead')
            ->where(['userid'=>2])->get()->all();

        $experienceDetails = DB::table('experiences')
            ->selectRaw('officename, officeaddress, jobtype, designation, service, `group` , subgroup, ranklabel, fromdatebs, enddatebs, workingstatus, workingstatuslabel, remarks, document')
            ->where(['userid'=>2])->get()->all();   

        $data = [
            'profile' => $profile,
            'extraDetails' => $extraDetails,
            'contactDetails' => $contactDetails[0] ?? '',
            'educationDetails' => $educationDetails,
            'trainingDetails' => $trainingDetails,
            'experienceDetails' => $experienceDetails
        ];

        return view('admin.pages.preview.preview',$data);
    }

   /**
    * A function that is used to add school setup data.
    * 
    * @param Request request The request object.
    */
    public function submit(Request $request)
    {  
        $post = $request->all();

        $sql = "SELECT
                    designation,
                    servicesgroup,
                    jobcategory,
                    D.title,
                    servicegroupname,
                    vacancynumber,
			        numberofvacancy,
                    name
                FROM
                    vacancies AS V
                    JOIN designations AS D ON D.id = V.designation
                    JOIN servicegroups AS S ON S.id = V.servicesgroup
                    JOIN jobcategories AS J ON J.id = V.jobcategory
                WHERE
                    V.jobstatus = 'Active' ";

        $vacancies = DB::select($sql);
        $vacancy = [];
        if(!empty($vacancies)){
            foreach ($vacancies as $key => $value) {
                $vacancy[$value->title.'-'.$value->servicegroupname]['designation'] = $value->designation;
                $vacancy[$value->title.'-'.$value->servicegroupname]['servicesgroup'] = $value->servicesgroup;
                $vacancy[$value->title.'-'.$value->servicegroupname]['vacancycode'][] = $value->vacancynumber;
                $vacancy[$value->title.'-'.$value->servicegroupname]['jobcat'][$value->name][] = $value;
            }
        }
        //dd($vacancy);

        // $vacancy= Vacancy::where('jobstatus','Active')->get();
        return view('admin.pages.preview.submit',compact('vacancy'));
    }
}
