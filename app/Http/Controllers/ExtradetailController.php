<?php

namespace App\Http\Controllers;

use App\Models\Extradetail;
use Exception;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ExtradetailController extends Controller
{

    public function otherdetailForm(Request $request)
    {
        $post = $request->all();
        $previousData=[];


        // dd($post['personaldetailid']);
        //dd(session()->get('personalid'));

        $tabledata=DB::table('extradetails')->where('personalid',$post['personaldetailid'])->first();
        if($tabledata){
            $previousData=$tabledata;
        }
        $data=[
            'previousData'=>@$previousData,
            'saveurl'=>route('storeExtraDetailsData'),
        ];
        return view('admin.pages.otherdetails.otherdetailssetup',$data);
    }

    public function storeExtraDetailsData(Request $request){
        try{
            $post=$request->all();

            $rules = [
                'cast' => 'required',
                'religion' => 'required',
                // 'religionother' => 'required',
                'maritalstatus' => 'required',
                'employmentstatus' => 'required',
                'motherlanguage'=>'required',
                'disabilitystatus'=>'required',         
                // 'fatherandmotheroccupation'=>'required',
                // 'fatherandmotheroccupationother'=>'required',
                // 'whatyouwantoyourself'=>'required',
                // 'castgroup' => 'required',
            ];

            $messages = [
                'cast.required' => 'तपाईले जात भर्नुभएको छैन |',
                'religion.required' => 'तपाईले धर्म भर्नुभएको छैन |',
                'maritalstatus.required' => 'तपाईले वैवाहिक स्थिति भर्नुभएको छैन |',
                'employmentstatus.required' => 'तपाईले रोजगारी अवस्था भर्नुभएको छैन |',
                'motherlanguage.required' => 'तपाईले मात्री भाषा भर्नुभएको छैन |',
                'disabilitystatus.required' => 'तपाईले असक्षमता स्थिति भर्नुभएको छैन |',
                // 'fatherandmotheroccupation.required' => 'तपाईले बुबा र आमाको पेशा भर्नुभएको छैन |',
                // 'fatherandmotheroccupationother.required' => 'तपाईले बुबा र आमाको अन्य पेशा भर्नुभएको छैन |',
                // 'whatyouwantoyourself.required' => 'तपाईले आफ्नै आवश्यकता भर्नुभएको छैन |',
                // 'castgroup.required' => 'तपाईले जातको समूह भर्नुभएको छैन |',
            ];

            $validator = Validator::make($request->all(), $rules, $messages);
            if ($validator->fails()) {
                throw new Exception($validator->errors()->first(), 1);
            }

            $type="success";
            $message="Extra Details has been saved Successfully.";

            $response=Extradetail::storeExtraDetails($post);

            $response=[
                "redirectUrl"=>"contactdetailForm",
                "success"=>true,
                "message"=>$message,
            ];

        }catch(Exception $e){
            $message=$e->getMessage();
            $response=[
                "redirectUrl"=>"otherdetailForm",
                "success"=>false,
                "message"=>$message,
            ];

        }
        echo json_encode(array($response));
    }
}
