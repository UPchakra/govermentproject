<?php

namespace App\Http\Controllers;

use App\Models\Education;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\DB;
use Image;
use Validator;

class EducationController extends Controller
{
    //
    public function index()
    {
        
        return view('admin.pages.education.vieweducationsetup');
    }
 
   /**
    * A function that is used to add school setup data.
    *
    * @param Request request The request object.
    */
    public function educationForm(Request $request)
    {
        $post = $request->all();
        $academics = DB::table('academics')->where('status','Y')->get();
        $personalDetails=DB::table('personals')->select('id')->where('userid',auth()->user()->id)->first();
        if(!empty(@$post['educationdetailid'])){
            $previousData=Education::previousAllData($post);
                }
        $data=[
            'previousData'=>@$previousData,
            'saveurl'=>route('storeEducationDetails'),
            'personalid'=>@$personalDetails->id,
            'academics' => $academics,

        ];

        return view('admin.pages.education.addeducationsetup',$data);
    }

    public function storeEducationDetails(Request $request){
        try{
            $post=$request->all();
            $rules = [
                'universityboardname' => 'required',
                'educationlevel' => 'required',
                'educationfaculty' => 'required',
                'educationinstitution' => 'required',
                'devisiongradepercentage' => 'required',
                'mejorsubject' => 'required',
                'educationaltype' => 'required',
                'qulificationawardeddetails' => 'required',
                'passoutdatead' => 'required',
                'passoutdatebs' => 'required',
               
            ];

            $messages =  [
                'universityboardname.required' => 'तपाईले विश्वविद्यालयको नाम भर्नुभएको छैन |',
                'educationlevel.required' => 'तपाईले शिक्षाको स्तर भर्नुभएको छैन |',
                'educationfaculty.required' => 'तपाईले शिक्षाको संकाय भर्नुभएको छैन |',
                'educationinstitution.required' => 'तपाईले शिक्षाको संस्था भर्नुभएको छैन |',
                'devisiongradepercentage.required' => 'तपाईले शिक्षाको डिभिजन/ग्रेड/प्रतिशत भर्नुभएको छैन |',
                'mejorsubject.required' => 'तपाईले प्रमुख विषय भर्नुभएको छैन |',
                'educationaltype.required' => 'तपाईले शिक्षा को प्रकार भर्नुभएको छैन |',
                'qulificationawardeddetails.required' => 'तपाईले योग्यता पुरस्कृत विवरण भर्नुभएको छैन |',
                'passoutdatead.required' => 'तपाईले पासआउट मिति (अंग्रेजीमा) भर्नुभएको छैन |',
                'passoutdatebs.required' => 'तपाईले पासआउट मिति (नेपालीमा) भर्नुभएको छैन |',
             
            ];

            $validator = Validator::make($request->all(), $rules, $messages);
            if ($validator->fails()) {
                throw new Exception($validator->errors()->first(), 1);
            }
 
            $type="success";
            $message="Education Details has been saved Successfully.";
            $post['academicdocument']=[];
            $post['equivalentdocuments']=[];
            if ($request->hasFile('academicdocument')) {

                $image_tmp = $request->file('academicdocument');
                foreach ($image_tmp as $id=>$file) {
                    if ($file->isValid()) {
                        $extension = $file->getClientOriginalExtension();
                        $originalFilename = explode('.',$file->getClientOriginalName())[0];
                        $current = date('Ymd');
                        $filename = $originalFilename.'-'.$current.'-' . rand(111, 99999) . '.' . $extension;
                        $image_path = public_path('uploads/education/' . $filename);
                        $pdf_path = public_path('uploads/education/');
    
    
                        if($extension=="pdf"||$extension=="pdf"){
                            $file->move($pdf_path, $filename);
    
                        }else{
                            Image::make($file)->save($image_path);
                        }
                        $post['academicdocument'][$id] = $filename;
                    }

                }

                $post['academicdocument']=json_encode( $post['academicdocument']);

            }
            if ($request->hasFile('equivalentdocument')) {
                $image_tmp = $request->file('equivalentdocument');
                if ($image_tmp->isValid()) {
                    $extension = $image_tmp->getClientOriginalExtension();
                    $filename = 'equivalentdocument' . rand(111, 99999) . '.' . $extension;
                    $image_path = public_path('uploads/education/' . $filename);
                    $pdf_path = public_path('uploads/education/');
                    if($extension=='pdf'||$extension=='PDF'){
                        $image_tmp->move($pdf_path, $filename);
                    }else{
                        Image::make($image_tmp)->save($image_path);

                    }
                    $post['equivalentdocuments'] = $filename;
                }
            }
            $response=Education::storeEducationDetails($post);

        }catch(Exception $e){
            $response=false;

            $type='errro';
            $message=$e->getMessage();

        }
        echo json_encode(array('type'=>$type,'message'=>$message,'response'=>$response));

    }

    public function getEducationDetailsData(Request $request)
    {
        $post = $request->all();
        $post['userid']=auth()->user()->id;
        $data = Education::getEducationDetailsData($post);
        $i = 0;
        $array = array();
        $filtereddata = (@$data["totalfilteredrecs"] > 0 ? $data["totalfilteredrecs"] : @$data["totalrecs"]);
        $totalrecs = @$data["totalrecs"];

        unset($data["totalfilteredrecs"]);
        unset($data["totalrecs"]);
        foreach ($data as $row) {
            $array[$i]["sn"] = $i + 1 ;
            $array[$i]["universityboardname"] = $row->universityboardname;
            $array[$i]["educationlevel"] = $row->name;
            $array[$i]["educationfaculty"] = $row->educationfaculty;
            $array[$i]["devisiongradepercentage"] = $row->devisiongradepercentage;
            $array[$i]["mejorsubject"] = $row->mejorsubject;
            $array[$i]["passoutdatead"] = $row->passoutdatead;
            $array[$i]["educationaltype"] = $row->educationaltype;
            $images=json_decode(@$row->academicdocument);
            $urls='';
            if(!empty($images)){
            foreach ($images as $image){
                $urls .= '<a style="padding-left:10px;" title="'.@$image.'" href="'.asset('uploads/education/' . @$image).'" download><i class="fa fa-download"></i></a>';
            }}
            
            $array[$i]["academicdocument"] =$urls;

            $array[$i]["equivalentdocument"] ='<a href="'.asset('uploads/education/' . @$row->equivalentdocument).'" download><i class="fa fa-download"></i></a>';
            $action = "";
            // if($this->isedit){
            $action .= '<a href="javascript:;" class="editEducationSetup"  data-educationdetail="' . $row->id . '"  data-user="' . $row->userid . '"  data-personalid="'.$row->personalid.'"><i class="fa fa-edit">EDIT</i></a>';
            // }
            // if($this->isdelete){
            $action .= '&nbsp | &nbsp <a href="javascript:;" class="deleteEducationDetail"  data-educationdetail="' . $row->id . '" data-user="' . $row->userid . '"  data-personalid="'.$row->personalid.'"><i class="fa fa-trash">DELETE</i></a>';
            // }
            $array[$i]["action"] = $action;
            $i++;
        }
        if (!$filtereddata) {
            $filtereddata = 0;
        }
        if (!$totalrecs) {
            $totalrecs = 0;
        }

        echo json_encode(array("recordsFiltered" => @$filtereddata, "recordsTotal" => @$totalrecs, "data" => $array));
        exit;

    }

    public function deleteEducationDetailsData(Request $request)
    {
        try {
            $type = 'success';
            $message = 'Education Details has been Deeted Successfully.';
            $post = $request->all();
            $response = Education::deleteEducationDetailsData($post);
        } catch (Exception $e) {
            $type = 'error';
            $message = $e->getMessage();
            $response = false;
        }
        echo json_encode(['type' => $type, 'message' => $message]);
    }
}
