<?php

namespace App\Http\Controllers;

use App\Models\Personal;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;

class PersonalController extends Controller
{

   /**
    * A function that is used to add school setup data.
    *
    * @param Request request The request object.
    */
    public function personalForm(Request $request)
    {
        $post=$request->all();
        $provinces=DB::table('provinces')->where('status','Y')->get();
        $districts=DB::table('districts')->where('status','Y')->get();
        $previousData=[];

        // echo @$post['personaldetailid'].' --- ';
        // echo @$post['userid'].' - ';
        if(!empty(@$post['personaldetailid'] && @$post['userid'])){
            $previousData=Personal::previousAllData($post);
        }
        $data=[
            'previousData'=>@$previousData,
            'provinces'=>$provinces,
            'districts'=>$districts,
            'saveurl'=>route('storePersonalDetails'),
        ];

        return view('admin.pages.personal.viewpersonalsetup',$data);
    }


    public function storePersonalDetails(Request $request){
        try{
            $post=$request->all();

            $rules = [
                'nfirstname' => 'required',
                'nlastname' => 'required',
                'efirstname'=>'required',
                'elastname'=>'required',
                'dateofbirthbs' => 'required',
                'dateofbirthad' => 'required',
                'gender' => 'required',
                'fatherfirstname' => 'required',
                'fatherlastname'=>'required',
                'motherfirstname'=>'required',
                'motherlastname' => 'required',
                'grandfatherfirstname' => 'required',
                'grandfatherlastname'=>'required',
                'citizenshipnumber'=>'required',
                'citizenshipissuedistrictid'=>'required',
                'citizenshipissuedate'=>'required',
            ];

            $messages = [
                'nfirstname.required' => 'तपाईले पहिलो नाम (नेपालीमा) भर्नुभएको छैन |',
                'nlastname.required' => 'तपाईले थर (नेपालीमा) भर्नुभएको छैन |',
                'efirstname.required' => 'तपाईले पहिलो नाम (अंग्रेजीमा) भर्नुभएको छैन |',
                'elastname.required' => 'तपाईले थर (अंग्रेजीमा) भर्नुभएको छैन |',
                'dateofbirthbs.required' => 'तपाईले जन्म मिति (नेपालीमा) भर्नुभएको छैन |',
                'dateofbirthad.required' => 'तपाईले जन्म मिति (अंग्रेजीमा) भर्नुभएको छैन |',
                'gender.required' => 'तपाईले लिङ्ग भर्नुभएको छैन |',
                'fatherfirstname.required' => 'तपाईले बुवाको पहिलो नाम भर्नुभएको छैन |',
                'fatherlastname.required' => 'तपाईले बुवाको थर भर्नुभएको छैन |',
                'motherfirstname.required' => 'तपाईले आमाको पहिलो नाम भर्नुभएको छैन |',
                'motherlastname.required' => 'तपाईले आमाको थर भर्नुभएको छैन |',
                'grandfatherfirstname.required' => 'तपाईले हजुरबुबाको पहिलो नाम भर्नुभएको छैन |',
                'grandfatherlastname.required' => 'तपाईले हजुरबुबाको थर भर्नुभएको छैन |',
                'citizenshipnumber.required' => 'तपाईले नागरिकता नम्बर भर्नुभएको छैन |',
                'citizenshipissuedistrictid.required' => 'तपाईले नागरिकता मुद्दा जिल्ला भर्नुभएको छैन |',
                'citizenshipissuedate.required' => 'तपाईले नागरिकता जारी मिति भर्नुभएको छैन |',

            ];

            $validator = Validator::make($request->all(), $rules, $messages);
            if ($validator->fails()) {
                throw new Exception($validator->errors()->first(), 1);
            }

            $type="success";
            $message="Personal Details has been saved Successfully.";

            $response=Personal::storePersonalDetails($post);
            if($response['success']){ 
                $message="Personal Details has been saved Successfully.";
                $response=[
                    "redirectUrl"=>"otherdetailForm",
                    "success"=>true,
                    "message"=>$message,
                    "personalid"=>$response['id']
                ];
            }
        }catch(Exception $e){
            $response=[
                "redirectUrl"=>"personal",
                "success"=>false,
                "message"=>$e->getMessage()
            ];

        }
        echo json_encode(array($response));
    }

}
