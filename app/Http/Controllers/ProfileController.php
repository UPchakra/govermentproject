<?php

namespace App\Http\Controllers;

use App\Models\Profile;
use App\Models\User;
use Exception;
use Illuminate\Http\Request;
use Image;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class ProfileController extends Controller
{
    public function profile()
    {
        $useremail = Auth::user()->email;
        $profiles = Profile::where('email', $useremail)->first();
        dd($profiles);

        $data = [
            'profiles' => $useremail,
        ];
        return view('admin.profile.index', $useremail);
    }
    public function updateProfile(Request $request)
    {
        $profiles = DB::table('profiles')->where('userid', Auth::user()->id)->first();
        $data = [
            'profiles' => $profiles,
            'save_url' => route('storeProfile'),
            'userid' => Auth::user()->id,
        ];
        return view('admin.profile.profileupdate', $data);
    }

    public function passwordSettings(Request $request)
    {
        $data = [
            'saveurl' => route('passwordUpdate'),
            'userid' => Auth::user()->id,
        ];
        return view('admin.profile.passwordchange',$data);
    }
// update profile data
    public function storeProfile(Request $request)
    {
        try {
            $post = $request->all();
            $type = 'success';
            $message = 'Profile Updated Successfully';

            if ($request->hasFile('image')) {
                $image_tmp = $request->file('image');
                if ($image_tmp->isValid()) {
                    $extension = $image_tmp->getClientOriginalExtension();
                    $filename = 'profileimage_' . rand(111, 99999) . '.' . $extension;
                    $image_path = public_path('uploads/profile/' . $filename);
                    Image::make($image_tmp)->save($image_path);
                    $post['image'] = $filename;
                }
            }
            $response = Profile::storeProfileData($post);
        } catch (Exception $e) {
            $type = 'error';
            $message = $e->getMessage();
            $response = false;
        }
        echo json_encode(['type' => $type, 'message' => $message]);
    }
    
   // Checking User Current Password
    public function chkUserPassword(Request $request)
    {
        $post = $request->all();
        $current_password = $post['current_password'];
        $user_id = Auth::guard('web')->user()->id;
        $check_password = User::where('id', $user_id)->first();
        if (Hash::check($current_password, $check_password->password)) {
            return "true";
            die;
        } else {
            return "false";
            die;
        }
    }

    // Updating Password
    public function passwordUpdate(Request $request)
    {


        try {
            $post = $request->all();
            // dd($post);
            $type = 'success';
            $message = 'Password Updated Successfully';
            $response = Profile::updatePasswordDatas($post);
        } catch (Exception $e) {
            $type = 'error';
            $message = $e->getMessage();
            $response = false;
        }
        echo json_encode(['type' => $type, 'message' => $message]);
    }
}
