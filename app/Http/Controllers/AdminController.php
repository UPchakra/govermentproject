<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Vacancy;
use Exception;
use Illuminate\Contracts\Session\Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Image;
use App\Mail\ForgetPassword;
use App\Mail\VerifyEmail;

class AdminController extends Controller
{
    public function dashboard()
    {       
        return view('admin.layouts.admin_designs');
    }

    public function dashboardDetails()
    {
        $data['newvac'] = Vacancy::getCurrentVacancy();
        // $data['appliedpost'] = Vacancy::getAppliedVacancy();
        // dd($data);
        return view('admin.dashboard', $data);
    }


    public function login()
    {
        if (empty(session()->has('LoginSession'))) {
            return view('admin.auth.login');
        } else {
            return redirect()->route('dashboard');
        }
        return view('admin.auth.login');
    }

    //login
    // public function loginadmin(Request $request){
    //     $post=$request->all();
    //     $this->validate($request, [
    //         'email' => 'required|email',
    //         'password' => 'required',
    //     ]);
    //     if(auth()->attempt(['email' => $post['email'], 'password' => $post['password']])){
    //      $request->session()->put('LoginSession',$post['email']);
    //         return redirect('/dashboard');

    //     }else{
    //         return redirect()->back();
    //     }
    //     if(empty(session()->has('LoginSession'))){
    //         return view ('admin.auth.login');
    //     } else {
    //         return redirect()->route('dashboard');
    //     }

    // }

    public function loginadmin(Request $request)
    {
        $post = $request->all();
        $this->validate($request, [
            'email' => 'required',
            'password' => 'required',
        ]);

        $usersEmailCheck = DB::table('users')->where('email', $post['email'])->where('status', 'Y')->first();
        if (!empty($usersEmailCheck)) {

            if (auth()->attempt(['email' => $post['email'], 'password' => $post['password'], 'status' => 'Y'])) {
                $response = url('/') . ('/dashboard');
                $request->session()->put('LoginSession', $post['email']);
                $roleid = DB::table('userroles')->select('roleid')->where('userid', auth()->user()->id)->first();
                session()->put('roleid', $roleid->roleid);

                return redirect('/dashboard');
            } else {
                return redirect()->back();
            }
        } else {
            return redirect()->back();
        }

        if (empty($usersEmailCheck)) {

            return view('admin.auth.login');
        }
        if (empty(session()->has('LoginSession'))) {
            return view('admin.auth.login');
        } else {
            return redirect()->route('dashboard');
        }
    }

    //logout
    public function logout()
    {
        auth()->logout();
        session()->flush();
        return redirect('/');
    }

    public function userRegister()
    {

        if (session()->has('LoginSession')) {
            return redirect()->route('dashboard');
        }
        return view('admin.auth.register',);
    }

    public function refreshCaptcha()
    {
        $rand_num = rand(1111, 9999);
        session()->put('captchavalue', $rand_num);
        $layer = imagecreatetruecolor(60, 30);
        $captcha_bg = imagecolorallocate($layer, 255, 160, 120);
        imagefill($layer, 0, 0, $captcha_bg);
        $captcha_text_color = imagecolorallocate($layer, 0, 0, 0);
        $img = imagestring($layer, 5, 5, 5, $rand_num, $captcha_text_color);
        header('Content-Type:image/jpeg');
        imagejpeg($layer);
    }

    public function registerUser(Request $request)
    {
        try {
            $post = $request->all();
            $this->validate($request, [
                'email' => 'required|email',
                'password' => 'required',
                'captcha' => 'required'
            ]);
            if (session()->get('captchavalue') != $post['captcha']) {
                return redirect()->back()->with('flash_error', 'Captch does not match');
            }
            $usersCount = DB::table('users')->where('email', $post['email'])->count();
            if ($usersCount > 0) {
                return redirect()->back()->with('flash_error', 'Email Already Exists');
            }
            $otp = rand(000000, 999999);
            $userArray = [
                'email' => strtolower($post['email']),
                'password' => bcrypt($post['password']),
                'otp' => strval($otp),
            ];
            $userresultid = DB::table('users')->select('id')->orderBy('id', 'desc')->first();
            $lastuserid = 1;
            if (!empty($userresultid)) {
                $lastuserid = $userresultid->id + 1;
            }
            $profileArray = [
                'firstname' => $post['firstname'],
                'lastname' => $post['lastname'],
                'middlename' => $post['middlename'],
                'userid' => $lastuserid,
                'gender' => $post['gender'],
                'contactnumber' => $post['contactnumber'],
                'email' => strtolower($post['email']),
                'createdatetime' => date('Y-m-d H:i:s'),
            ];
            $userRoleInsert = [
                'userid' => $lastuserid,
                'roleid' => 2,
                'createdatetime' => date('Y-m-d H:i:s'),
            ];

            $userResponse = User::create($userArray);
            $profileResponse = DB::table('profiles')->insert($profileArray);
            DB::table('userroles')->insert($userRoleInsert);
            $authenticated = Auth::login($userResponse);
            if ($authenticated) {
                session()->put('LoginSession', $userArray['email']);
                session()->put('roleid', 2);
            }

            Mail::to($post['email'])->send(new VerifyEmail($post['email'], $otp));
            return redirect()->route('verifyregisteredemail', ['email' => $post['email']]);
        } catch (Exception $e) {
            return false;
            throw $e;
        }
    }

    public function registerVerify($email) {
        return view('admin.auth.verifyEmail', ['email' => $email]);
    }

    public function registerVerifyOtp(Request $request) {
        $request->validate([
            'otp' => 'required'
        ]);
        $user = User::where(['email' => $request->registeredemail, 'otp' => $request->otp])->first();
        return redirect()->route('dashboard');
    }

    public function forgotPassword(Request $request)
    {
        if ($request->isMethod('post')) {
            try {
                $type = 'success';
                $message = "Succefully Requested. Please check your registered mail for new Password";
                $randompassword = Str::random(8);
                $useremail = $request->email;
                $password = Hash::make($randompassword);
                $user = User::where('email', $useremail)->first();
                if(!$user) {
                    throw new Exception('User does not exist.', 1);
                }
                $user->password = $password;
                $user->save();
                $mailSent = Mail::to($user->email)->send(new ForgetPassword($useremail, $randompassword));
            } catch (Exception $e) {
                $type = 'error';
                $message = $e->getMessage();
            }

            $save_url = route('storeProfile');
            echo json_encode(array('type' => $type, 'message' => $message, 'url' => $save_url));
            exit;
        }
        return view('admin.auth.forgotPassword');
    }
}
