<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CommonController extends Controller
{
    public function getDistrict(Request $request)
    {
        $post = $request->all();
        $result = DB::table('districts')->where('provinceid', $post['provinceid'])->get();
         $html='<option value="" selected>जिल्ला छान्नुहोस्</option>';
        //  $html.='<option value="">h=sas छान्नुहोस्</option>';

        foreach ($result as $key => $value) {
          $html .= "<option value='" . $value->id . "'>" . $value->districtname . "</option>";
        }
        return $html;
    }

    public function getVdcorMunicipality(Request $request)
    {
        $post = $request->all();
        $result = DB::table('vdcormunicipalities')->where('districtid', $post['districtid'])->get();
         $html='<option value="" selected>पालिकाको नाम छान्नुहोस्</option>';
        foreach ($result as $key => $value) {
          $html .= "<option value='" . $value->id . "'>" . $value->vdcormunicipalitiename . "</option>";
        }
        return $html;
    }
}
