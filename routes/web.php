<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\ApplyJobsController;
use App\Http\Controllers\CommonController;
use App\Http\Controllers\ContactDetailsController;
use App\Http\Controllers\DocumentController;
use App\Http\Controllers\EducationController;
use App\Http\Controllers\ExperienceController;
use App\Http\Controllers\ExtradetailController;
use App\Http\Controllers\OtherdetailsController;
use App\Http\Controllers\PaymentController;
use App\Http\Controllers\PersonalController;
use App\Http\Controllers\PreviewController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\TrainingController;
use App\Http\Controllers\VacancyController;
use PhpParser\Node\Expr;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::prefix('/')->group(function () {
    // =========================================Login=======================================================
    Route::match(['get', 'post'], '/', [AdminController::class, 'login'])->name('login');
    Route::match(['get', 'post'], '/login', [AdminController::class, 'login'])->name('login');
    Route::post('/adminlogin', [AdminController::class, 'loginadmin'])->name('loginadmin');
    Route::get('/register', [AdminController::class, 'userRegister'])->name('userRegister');
    Route::any('register/user', [AdminController::class, 'registerUser'])->name('registerUser');
    Route::get('/register/otp/{email}', [AdminController::class, 'registerVerify'])->name('verifyregisteredemail');
    Route::post('/register/otp/verify', [AdminController::class, 'registerVerifyOtp'])->name('verifyRegisterUserOtp');
    Route::get('refresh_captcha', [AdminController::class, 'refreshCaptcha'])->name('refresh_captcha');

    // =========================================End Login=======================================================

    Route::group(['middleware' => ['auth']], function () {

        // =========================================Dashboard==========================================================
        Route::match(['get', 'post'], '/dashboard', [AdminController::class, 'dashboard'])->name('dashboard');
        Route::post('/dashboardDetails', [AdminController::class, 'dashboardDetails'])->name('dashboardDetails');
        // =========================================End Dashboard=======================================================

        // =========================================Personal Setup==========================================================
        Route::match(['post'], '/personal', [PersonalController::class, 'personalForm'])->name('personal');
        Route::match(['get', 'post'], '/storepersonal/details', [PersonalController::class, 'storePersonalDetails'])->name('storePersonalDetails');
        // =========================================End Personal Setup=======================================================

        // =========================================Other Personal Setup==========================================================
        Route::match(['post'], '/otherdetailForm', [ExtradetailController::class, 'otherdetailForm'])->name('otherdetailForm');
        Route::post('/otherdetail/store', [ExtradetailController::class, 'storeExtraDetailsData'])->name('storeExtraDetailsData');
        // =========================================Other Personal Setup==========================================================


        // =========================================Contact Details==========================================================

        Route::match(['get', 'post'], '/contactdetailForm', [ContactDetailsController::class, 'contactdetailForm'])->name('contactdetailForm');
        Route::match(['post'], '/contactdetail/store', [ContactDetailsController::class, 'storeContactDetails'])->name('storeContactDetails');

        // =========================================End Contact Setup=======================================================



        // =========================================Document Setup==========================================================
        Route::match(['post'], '/document', [DocumentController::class, 'index'])->name('document');
        Route::match(['get', 'post'], '/document/form', [DocumentController::class, 'documentForm'])->name('documentForm');
        // =========================================End Document Setup=======================================================

        // =========================================Education Setup==========================================================
        Route::match(['post'], '/education', [EducationController::class, 'index'])->name('education');
        Route::match(['get', 'post'], '/education/form', [EducationController::class, 'educationForm'])->name('educationForm');
        Route::match(['get', 'post'], '/store/educationdetails', [EducationController::class, 'storeEducationDetails'])->name('storeEducationDetails');
        Route::match(['get', 'post'], '/geteducationdetails', [EducationController::class, 'getEducationDetailsData'])->name('getEducationDetailsData');
        Route::any('/educationdetails/delete', [EducationController::class, 'deleteEducationDetailsData'])->name('deleteEducationDetailsData');
        // =========================================End Education Setup=======================================================

        // =========================================Training Setup==========================================================
        Route::match(['post'], '/training', [TrainingController::class, 'index'])->name('training');
        Route::match(['get', 'post'], '/training/form', [TrainingController::class, 'trainingForm'])->name('trainingForm');
        Route::match(['get', 'post'], '/store/trainingdetails', [TrainingController::class, 'storeTrainingDetails'])->name('storeTrainingDetails');
        Route::match(['get', 'post'], '/gettrainingdetails', [TrainingController::class, 'getTrainingDetailsData'])->name('getTrainingDetailsData');
        Route::any('/trainingdetails/delete', [TrainingController::class, 'deleteTrainingDetailsData'])->name('deleteTrainingDetailsData');
        // =========================================End Training Setup=======================================================

        // =========================================Experiene Setup==========================================================
        Route::match([ 'post'], '/experience', [ExperienceController::class, 'index'])->name('experience');
        Route::match(['get', 'post'], '/experience/form', [ExperienceController::class, 'experienceForm'])->name('experienceForm');
        Route::match(['get', 'post'], '/store/experiencedetails', [ExperienceController::class, 'storeExperienceDetails'])->name('storeExperienceDetails');
        Route::match(['get', 'post'], '/getexperiencedetails', [ExperienceController::class, 'getExperienceDetailsData'])->name('getExperienceDetailsData');
        Route::any('/experiencedetails/delete', [ExperienceController::class, 'deleteExperienceDetailsData'])->name('deleteExperienceDetailsData');
        // =========================================End Training Setup=======================================================

        // =========================================Document Setup==========================================================
        Route::match(['post'], '/document', [DocumentController::class, 'index'])->name('document');
        Route::match(['get', 'post'], '/document/form', [DocumentController::class, 'documentForm'])->name('documentForm');
        Route::match(['post'], '/document/store', [DocumentController::class, 'storeDocumentDetails'])->name('documentStore');
        // =========================================End Document Setup=======================================================

        // =========================================Preview and Submit Setup==========================================================
        Route::match(['post'], '/preview', [PreviewController::class, 'preview'])->name('preview');
        Route::match(['get', 'post'], '/submit', [PreviewController::class, 'submit'])->name('submit');
        // =========================================End Document Setup=======================================================
        // =========================================Apply Jobs Setup==========================================================
        Route::match(['get','post'], '/applyJobsForm', [ApplyJobsController::class, 'applyJobsForm'])->name('applyJobsForm');
        Route::post('/getjobdetails', [ApplyJobsController::class, 'getjobdetails'])->name('getjobdetails');
        Route::post('/storeApplyJobsDetails', [ApplyJobsController::class, 'storeApplyJobsDetails'])->name('storeApplyJobsDetails');
        // =========================================End Apply Jobs Setup=======================================================

        // =========================================Profile=============================================================
        Route::match(['get', 'post'], '/profile', [ProfileController::class, 'profile'])->name('profile');
        // =========================================End Profile=========================================================

        // =========================================Profile Update======================================================

        Route::match(['get', 'post'], 'update/profile', [ProfileController::class, 'updateProfile'])->name('updateProfile');
        Route::match(['get', 'post'], 'store/profile', [ProfileController::class, 'storeProfile'])->name('storeProfile');
        // =========================================End Profile Update===================================================

        // =========================================Password Setting=====================================================
        Route::match(['get', 'post'], 'password/setting', [ProfileController::class, 'passwordSettings'])->name('passwordSettings');
        // Checking Password
        Route::post('/update/password/check-password', [ProfileController::class, 'chkUserPassword'])->name('chkUserPassword');
        // Updating Password
        Route::post('update/password', [ProfileController::class, 'passwordUpdate'])->name('passwordUpdate');
        // =========================================End Password Setting==================================================



        //==========================================Province wise district===============================================================
        Route::post('/provincewisedistrict', [CommonController::class, 'getDistrict'])->name('getDistrict');

        //==========================================End Province wise district===============================================================
        //==========================================District wise vdcormunicipality===============================================================
        Route::any('/districtwisevdcormunicipality', [CommonController::class, 'getVdcorMunicipality'])->name('getVdcorMunicipality');

        //==========================================End District wise vdcormunicipality===============================================================


        // =========================================Vancacy Setup==========================================================
        Route::match(['get', 'post'], '/vancacysetup', [VacancyController::class, 'index'])->name('vancacy');
        Route::match(['get', 'post'], '/vancacysetup/form', [VacancyController::class, 'vacancyForm'])->name('vacancyForm');
        Route::match(['get', 'post'], '/store/vancacysetup', [VacancyController::class, 'storeVacancyDetails'])->name('storeVacancyDetails');
        Route::match(['get', 'post'], '/getvancacysetuplist', [VacancyController::class, 'getVacancyDetailsData'])->name('getVancacyDetailsData');
        Route::any('/vancacysetup/delete', [VacancyController::class, 'deleteVacancyDetailsData'])->name('deleteVacancyDetailsData');
        // =========================================End Vancacy Setup=======================================================
        // =========================================Khalti=======================================================
        Route::any('/payment',[PaymentController::class, 'paymentKhalti']);
        Route::post('/khalti/verify/payment',[PaymentController::class, 'VerifyKhaltiPayment']);
	    Route::any('/khalti/store_payment', [PaymentController::class,'StorePayment']);
        // =========================================End Khalti=======================================================

           // =========================================E-sewa=======================================================
           Route::any('/payment-verify',[
            'uses'=>'PaymentController@verifyEsewa',
            'as' => 'payment.verify',]);
           Route::post('/khalti/verify/payment',[PaymentController::class, 'VerifyKhaltiPayment']);
           Route::any('/khalti/store_payment', [PaymentController::class,'StorePayment']);
           // =========================================End E-sewa=======================================================

    });
    // =========================================Forgot password====================================================================
    Route::match(['get', 'post'], '/forgotPassword', [AdminController::class, 'forgotPassword'])->name('forgotPassword');

    // =========================================Logout====================================================================
    Route::match(['get', 'post'], '/logout', [AdminController::class, 'logout'])->name('logout');
    //========================================End Logout==================================================================
});

Route::get('/{any}', function ($any) {

    return redirect()->route('dashboard');
})->where('any', '.*');
