<head>
	<base href="../../">

	{{-- <title>@yield('siteTitle')- {{ config('app.name') }}</title> --}}
	<title>@yield('siteTitle')- कर्मचारी सञ्चय कोष</title>
    <meta charset="utf-8" />
    <meta name="csrf-token" content="{{ csrf_token() }}" />    
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta property="og:locale" content="en_US" />
    <meta property="og:type" content="article" />
    <meta property="og:title" content="" />
    <meta property="og:url" content="https://keenthemes.com/metronic" />
    <meta property="og:site_name" content="Keenthemes | Metronic" />
    <link rel="canonical" href="https://preview.keenthemes.com/metronic8" />
    <link rel="shortcut icon" href="assets/media/logos/favicon.ico" />
    <!--begin::Fonts-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
    <!--end::Fonts-->
    <!--begin::Page Vendor Stylesheets(used by this page)-->
    <link href="{{asset('adminAssets/assets/plugins/custom/datatables/datatables.bundle.css')}}" rel="stylesheet" type="text/css" />
    <!--end::Page Vendor Stylesheets-->
    <!--begin::Global Stylesheets Bundle(used by all pages)-->
    <link href="{{asset('adminAssets/assets/plugins/global/plugins.bundle.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('adminAssets/assets/css/style.bundle.css')}}" rel="stylesheet" type="text/css" />
   
    <link href="{{asset('adminAssets/assets/css/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('adminAssets/assets/css/bootstrap/css/bootstrap.css')}}" rel="stylesheet" type="text/css" />

    <link href="{{asset('adminAssets/assets/css/bootstrap/css/jquery.dataTables.min.css')}}" rel="stylesheet" type="text/css" />
 
    <link href="{{asset('adminAssets/assets/css/layout.css')}}" rel="stylesheet" type="text/css" />

    <link href="{{asset('adminAssets/assets/css/bootstrap/css/sweetalert.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('adminAssets/assets/css/jquery.timepicker.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('adminAssets/assets/css/nepali.datepicker.v2.2.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('adminAssets/assets/css/bootstrap-select.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('adminAssets/assets/css/select2.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('adminAssets/assets/css/main.css')}}" rel="stylesheet" type="text/css" />
	<link href="{{asset('adminAssets/assets/css/style.css')}}" rel="stylesheet" type="text/css" />
	<link href="{{asset('adminAssets/assets/css/main.scss')}}" rel="stylesheet" type="text/css" />
	<link href="{{asset('adminAssets/assets/css/main.css')}}" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css"
		integrity="sha512-KfkfwYDsLkIlwQp6LFnl8zNdLGxu9YAA1QvwINks4PhcElQSvqcyVLLD9aMhXd13uQjoXtEKNosOWaZqXgel0g=="
		crossorigin="anonymous" referrerpolicy="no-referrer" />
<style>
.successColor {
	color: green;
}

.errorColor {
	color: red;
}
</style>
</head>

@yield('css')
