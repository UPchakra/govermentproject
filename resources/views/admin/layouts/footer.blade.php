<head>
    <style>
        .footer{
            background-color: #505050;
            padding: 10px 0px !important;
            /* position: absolute;
            width: 100%;
            bottom: 0px; */
        }

        .footer .text-dark span{
color: !important;
        }
        .footer .text-dark a{
color: #ffffff8c !important;
font-weight: 600;
        }
    </style>
</head>

<!--begin::Footer-->
<div class="footer py-4 d-flex flex-lg-column" id="kt_footer">
    <!--begin::Container-->
    <div class="container d-flex flex-column flex-md-row align-items-center" style="justify-content: space-between;">
        <!--begin::Copyright-->
        <div class="text-dark order-2 order-md-1">
            <span class="text-muted fw-bold me-1">2022©</span>
            <a class="text-gray-800 text-hover-primary">Employees Provident Fund</a>
        </div>
        <div class="text-dark order-2 order-md-1">
            <a href="{{route('dashboard')}}"  class="text-gray-800 text-hover-primary">Code Logic Technologies Pvt. Ltd.</a>
        </div>
        <!--end::Copyright-->
               </div>
    <!--end::Container-->
</div>
<!--end::Footer-->