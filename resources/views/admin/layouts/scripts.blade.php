
<script>
    var hostUrl = "assets/";
    var token = "<?= csrf_token() ?>";
    var baseUrl = "<?= url('/') ?>";
</script>
{{-- Main Scripts

    <script src="{{ asset('adminAssets/assets/plugins/global/plugins.bundle.js') }}"></script>
    <script src="{{ asset('adminAssets/assets/js/scripts.bundle.js') }}"></script>
    
    <script src="{{ asset('adminAssets/assets/css/bootstrap/js/bootstrap.js') }}"></script>
    <script src="{{ asset('adminAssets/assets/css/bootstrap/js/bootstrap.bundle.js') }}"></script>
    <script src="{{ asset('adminAssets/assets/css/bootstrap/js/bootstrap.min.js') }}"></script>
    
    <script src="{{ asset('adminAssets/assets/js/jquery/jquery.js') }}"></script> 
    <script src="{{ asset('adminAssets/assets/js/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('adminAssets/assets/js/jquery/jquery.form.js') }}"></script>

<script src="{{ asset('adminAssets/assets/js/jquery/notify.js') }}"></script>
<script src="{{ asset('adminAssets/assets/js/jquery/notify.min.js') }}"></script>

<script src="{{ asset('adminAssets/assets/js/jquery/sweetalert.min.js') }}"></script>
<script src="{{ asset('adminAssets/assets/js/jquery/jquery.sweet-alert.custom.js') }}"></script>


{{-- End Main Scripts --}}

{{-- <script src="{{ asset('adminAssets/assets/js/bootstrap-select.js') }}"></script>
<script src="{{ asset('adminAssets/assets/js/select2.js') }}"></script>

<script src="{{ asset('adminAssets/assets/js/jquery.timepicker.min.js') }}"></script>


<script src="{{ asset('adminAssets/assets/js/jquery/jquery.dataTables.columnFilter.js') }}"></script>
<script src="{{ asset('adminAssets/assets/js/jquery/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('adminAssets/assets/plugins/custom/datatables/datatables.bundle.js') }}"></script>

<script src="{{ asset('adminAssets/assets/js/nepali.datepicker.v2.2.min.js') }}"></script>
<script src="{{ asset('adminAssets/assets/js/widgets.bundle.js') }}"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/js/fontawesome.min.js"
    integrity="sha512-5qbIAL4qJ/FSsWfIq5Pd0qbqoZpk5NcUVeAAREV2Li4EKzyJDEGlADHhHOSSCw0tHP7z3Q4hNHJXa81P92borQ=="
    crossorigin="anonymous" referrerpolicy="no-referrer"></script>


 --}}



{{-- Custom Scripts --}}

{{-- <script src="{{ asset('adminAssets/assets/js/custom/widgets.js') }}"></script>
<script src="{{ asset('adminAssets/assets/js/custom/apps/chat/chat.js') }}"></script>
<script src="{{ asset('adminAssets/assets/js/custom/utilities/modals/users-search.js') }}"></script>
<script src="{{ asset('adminAssets/assets/js/custom/apps/projects/settings/settings.js') }}"></script>
<script src="{{ asset('adminAssets/assets/js/custom/authentication/sign-in/general.js') }}"></script>

<script src="https://khalti.s3.ap-south-1.amazonaws.com/KPG/dist/2020.12.17.0.0.0/khalti-checkout.iffe.js"></script> --}}
{{-- End Custom Scripts --}}
{{-- <script>
    var hostUrl = "assets/";
</script>

<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>

<script>
    $(document).off('click', '#closedmodal');
    $(document).on('click', '#closedmodal', function() {
        var Mid = $('#closedmodal').val();
        $(`#${Mid}`).modal('hide');
    });
</script>

<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>
<script type="text/javascript">
    $(document).ready(function() {
        $(".toolbar a").click(function(event) {
            event.preventDefault();
        });
        
    });
</script>

@yield('scripts')  --}}


<!--begin::Global Javascript Bundle(used by all pages)-->
<script src="{{ asset('adminAssets/assets/plugins/global/plugins.bundle.js') }}"></script>
<script src="{{ asset('adminAssets/assets/js/scripts.bundle.js') }}"></script>
<!--end::Global Javascript Bundle-->
<!--begin::Page Vendors Javascript(used by this page)-->
<script src="{{ asset('adminAssets/assets/plugins/custom/datatables/datatables.bundle.js') }}"></script>
<!--end::Page Vendors Javascript-->
<!--begin::Page Custom Javascript(used by this page)-->
<script src="{{ asset('adminAssets/assets/js/widgets.bundle.js') }}"></script>
<script src="{{ asset('adminAssets/assets/js/custom/widgets.js') }}"></script>
<script src="{{ asset('adminAssets/assets/js/custom/apps/chat/chat.js') }}"></script>
{{-- <script src="{{asset('adminAssets/assets/js/custom/utilities/modals/upgrade-plan.js')}}"></script> --}}
{{-- <script src="{{asset('adminAssets/assets/js/custom/utilities/modals/create-app.js')}}"></script> --}}
<script src="{{ asset('adminAssets/assets/js/custom/utilities/modals/users-search.js') }}"></script>

<script src="{{ asset('adminAssets/assets/js/custom/apps/projects/settings/settings.js') }}"></script>

<script src="{{ asset('adminAssets/assets/js/custom/authentication/sign-in/general.js') }}"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/js/fontawesome.min.js"
    integrity="sha512-5qbIAL4qJ/FSsWfIq5Pd0qbqoZpk5NcUVeAAREV2Li4EKzyJDEGlADHhHOSSCw0tHP7z3Q4hNHJXa81P92borQ=="
    crossorigin="anonymous" referrerpolicy="no-referrer"></script>

 
<script src="{{ asset('adminAssets/assets/js/jquery/jquery.js') }}"></script>
<script src="{{ asset('adminAssets/assets/js/jquery/jquery.min.js') }}"></script>
<script src="{{ asset('adminAssets/assets/js/jquery/jquery.form.js') }}"></script>

<script src="{{ asset('adminAssets/assets/js/jquery/notify.js') }}"></script>
<script src="{{ asset('adminAssets/assets/js/jquery/notify.min.js') }}"></script>
{{-- datatables --}}
<script src="{{ asset('adminAssets/assets/js/jquery/jquery.dataTables.columnFilter.js') }}"></script>
<script src="{{ asset('adminAssets/assets/js/jquery/jquery.dataTables.min.js') }}"></script>

<script src="{{ asset('adminAssets/assets/js/jquery/sweetalert.min.js') }}"></script>
<script src="{{ asset('adminAssets/assets/js/jquery/jquery.sweet-alert.custom.js') }}"></script>
<script src="{{ asset('adminAssets/assets/js/jquery.timepicker.min.js') }}"></script>

<script src="{{ asset('adminAssets/assets/js/bootstrap-select.js') }}"></script>
<script src="{{ asset('adminAssets/assets/js/select2.js') }}"></script>
<script src="{{ asset('adminAssets/assets/css/bootstrap/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('adminAssets/assets/css/bootstrap/js/bootstrap.js') }}"></script>

<script src="{{ asset('adminAssets/assets/css/bootstrap/js/bootstrap.bundle.js') }}"></script>

<script src="{{ asset('adminAssets/assets/js/nepali.datepicker.v2.2.min.js') }}"></script>



<script src="https://khalti.s3.ap-south-1.amazonaws.com/KPG/dist/2020.12.17.0.0.0/khalti-checkout.iffe.js"></script>



<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>

<script>
    $(document).off('click', '#closedmodal');
    $(document).on('click', '#closedmodal', function() {
        var Mid = $('#closedmodal').val();
        $(`#${Mid}`).modal('hide');


    });
</script>

<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

</script>
<script type="text/javascript">
    $(document).ready(function() {
        $(".toolbar a").click(function(event) {
            event.preventDefault();
        });

    });
</script>

<script type="text/javascript">
    // function advertPop(designationid, servicesgroupid) {
    //     $("#vacancydetails").html()
    //     var data = {
    //         designationid: designationid,
    //         servicesgroupid: servicesgroupid,
    //     };
    //     var url = "{{ route('getjobdetails') }}";
    //     $.ajax({
    //         type: 'POST',
    //         url: url,
    //         data: data,
    //         headers: {
    //             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    //         },
    //         success: function(response) {
    //             $("#vacancydetails").html(response);
    //             $('#vacancydetailModal').modal('show');
    //         }
    //     });
    // }

</script>

@yield('scripts')
