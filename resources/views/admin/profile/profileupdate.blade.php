  <!--begin::Card header-->
  <div class="card-header border-0 cursor-pointer" role="button" data-bs-toggle="collapse"
      data-bs-target="#kt_account_profile_details" aria-expanded="true" aria-controls="kt_account_profile_details">
      <!--begin::Card title-->
      <div class="card-title m-0">
          <h3 class="fw-bolder m-0">Profile Details</h3>
      </div>
      <!--end::Card title-->
  </div>
  <!--begin::Card header-->
  <!--begin::Content-->
  <div id="kt_account_settings_profile_details" class="collapse show">
      <!--begin::Form-->
      <form id="profileUpdateForm"  method="post" action="{{$save_url}}" class="form" enctype="multipart/form-data">
        <input type="hidden" name="userid" value="{{@$userid}}"> 
		<!--begin::Card body-->
          <div class="card-body border-top p-9">
              <!--begin::Input group-->
              <div class="row mb-6">
                  <!--begin::Label-->
                  <label class="col-lg-4 col-form-label fw-bold fs-6">Profile Image</label>
                  <!--end::Label-->
                  <!--begin::Col-->
                  <div class="col-lg-8">
                      <!--begin::Image input-->
                      <div class="image-input image-input-outline" data-kt-image-input="true"
                          style="background-image: url({{asset('adminAssets/assets/media/svg/avatars/blank.svg')}})">
                          <!--begin::Preview existing avatar-->
                          <div class="image-input-wrapper w-125px h-125px"
                              style="background-image: url({{asset('adminAssets/assets/media/avatars/300-1.jpg')}})"></div>
                          <!--end::Preview existing avatar-->
                          <!--begin::Label-->
                          <label class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow"
                              data-kt-image-input-action="change" data-bs-toggle="tooltip" title="Change Image">
                              <i class="bi bi-pencil-fill fs-7"></i>
                              <!--begin::Inputs-->
                              <input type="file" name="image" accept=".png, .jpg, .jpeg" />
                              <input type="hidden" name="image_remove" />
                              <!--end::Inputs-->
                          </label>
                          <!--end::Label-->
                          <!--begin::Cancel-->
                          <span class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow"
                              data-kt-image-input-action="cancel" data-bs-toggle="tooltip" title="Cancel Image">
                              <i class="bi bi-x fs-2"></i>
                          </span>
                          <!--end::Cancel-->
                          <!--begin::Remove-->
                          <span class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow"
                              data-kt-image-input-action="remove" data-bs-toggle="tooltip" title="Remove Image">
                              <i class="bi bi-x fs-2"></i>
                          </span>
                          <!--end::Remove-->
                      </div>
                      <!--end::Image input-->
                      <!--begin::Hint-->
                      <div class="form-text">Allowed file types: png, jpg, jpeg.</div>
                      <!--end::Hint-->
                  </div>
                  <!--end::Col-->
              </div>
              <!--end::Input group-->
              <!--begin::Input group-->
              <div class="row mb-6">
                  <div class="col-lg-12">
                      <!--begin::Row-->
                      <div class="row">
                          <!--begin::Col-->
                          <div class="col-lg-4 fv-row">
							<label class="col-form-label fw-bold fs-6">First Name</label>

                              <input type="text" name="firstname" id="firstname" class="form-control form-control-lg form-control-solid mb-3 mb-lg-0"
                                  placeholder="First Name" value="{{@$profiles->firstname}}" />
                          </div>
                          <!--end::Col-->
                          <!--begin::Col-->
                          <div class="col-lg-4 fv-row">
							<label class="col-form-label  fw-bold fs-6">Middle Name</label>

                              <input type="text" name="middlename" id="middlename" class="form-control form-control-lg form-control-solid"
                                  placeholder="Middle Name" value="{{@$profiles->middlename}}" />
                          </div>
                          <!--end::Col-->

						   <!--begin::Col-->
						   <div class="col-lg-4 fv-row">
							<label class="col-form-label  fw-bold fs-6">Last Name</label>

							<input type="text" name="lastname" id="lastname" class="form-control form-control-lg form-control-solid"
								placeholder="Last Name" value="{{@$profiles->lastname}}" />
						</div>
						<!--end::Col-->
						  
                      </div>
                      <!--end::Row-->
                  </div>
                  <!--end::Col-->
              </div>
              <!--end::Input group-->

			   <!--begin::Input group-->
			   <div class="row mb-6">
				<div class="col-lg-12">
					<!--begin::Row-->
					<div class="row">
						<div class="col-lg-4 fv-row">
							<label class="col-form-label  fw-bold fs-6">Designation</label>
							<select name="designationid" class="form-control form-control-lg form-control-solid">
								<option value="" selected>Select Designation</option>
								<option value="1">1</option>
								<option value="2">2</option>
								<option value="3">3</option>
							</select>
						</div>
						<!--end::Col-->	
						<!--begin::Col-->
						<div class="col-lg-4 fv-row">
						  <label class="col-form-label fw-bold fs-6">Email</label>

							<input type="email" name="email" class="form-control form-control-lg form-control-solid"
								placeholder="Email" value="{{@$profiles->email}}" />
						</div>
						<!--end::Col-->

						 <!--begin::Col-->
						 <div class="col-lg-4 fv-row">
						  <label class="col-form-label fw-bold fs-6">Mobile Number</label>

						  <input type="number" name="contactnumber" class="form-control form-control-lg form-control-solid"
							  placeholder="Mobile Number" value="{{@$profiles->contactnumber}}" />
					  </div>
					  <!--end::Col-->
						
					</div>
					<!--end::Row-->
				</div>
				<!--end::Col-->
			</div>
			<!--end::Input group-->

			<!--begin::Input group-->
			<div class="row mb-6">
				<div class="col-lg-12">
					<!--begin::Row-->
					<div class="row">
						<!--begin::Col-->
						<div class="col-lg-4 fv-row">
							<label class="col-form-label  fw-bold fs-6">Gender</label>
							<select name="gender" class="form-control form-control-lg form-control-solid">
								<option value="" selected>Select Gender</option>
								<option value="Male" {{ @$profiles->gender == 'Male' ? 'selected' : '' }} >Male</option>
								<option value="Female" {{ @$profiles->gender == 'Female' ? 'selected' : '' }}>Female</option>
								<option value="Others" {{ @$profiles->gender == 'others' ? 'selected' : '' }}>Others</option>
							</select>
						</div>
						<!--end::Col-->
						<!--begin::Col-->
						<div class="col-lg-4 fv-row">
						  <label class="col-form-label fw-bold fs-6">Permanent Address</label>
							<input type="text" name="permanentaddress" id="permanentaddress" class="form-control form-control-lg form-control-solid"
								placeholder="Permanent Address" value="{{@$profiles->permanentaddress}}" />
						</div>
						<!--end::Col-->

						 <!--begin::Col-->
						 <div class="col-lg-4 fv-row">
						  <label class="col-form-label  fw-bold fs-6">Current Address</label>
						  <input type="text" name="currentaddress" id="currentaddress" class="form-control form-control-lg form-control-solid"
							  placeholder="Current Address" value="{{@$profiles->currentaddress}}" />
					  </div>
					  <!--end::Col-->
						
					</div>
					<!--end::Row-->
				</div>
				<!--end::Col-->
			</div>
			<!--end::Input group-->
          </div>
          <!--end::Card body-->
          <!--begin::Actions-->
          <div class="card-footer d-flex justify-content-end py-6 px-9">
              <button type="button" class="btn btn-primary" id="saveProfile">Update</button>
          </div>
          <!--end::Actions-->
      </form>
      <!--end::Form-->
  </div>
  <!--end::Content-->
  </div>
