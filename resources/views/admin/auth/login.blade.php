<!DOCTYPE html>
<html lang="en">
<!--begin::Head-->
@include('admin.layouts.head')
<!-- 03c75e green -->
<!-- #06c blue -->

<head>
	<style>

	</style>
</head>


<body id="kt_body" class="bg-dark">
	<div class="d-flex flex-column flex-root">
		<div class="d-flex flex-column flex-column-fluid bgi-position-y-bottom position-x-center bgi-no-repeat bgi-size-contain bgi-attachment-fixed"
			style="background-image: url(assets/media/illustrations/sketchy-1/14-dark.png">
			<div class="d-flex flex-center log_flx">
				<div class="log_img">
					<img src="<?php echo url('/') . '/adminAssets/assets/images/log_bk.png'; ?>" alt="">
				</div>
				<div class="bg-body shadow-sm log_form">
					<form class="form w-100" method="post" novalidate="novalidate" id="loginForm"
						action="{{route('loginadmin')}}">
						@csrf
						<div class="text-center mb-10 log_text_heading">
							<h1 class="head_txt">कर्मचारी सञ्चय कोष</h1>

						</div>
						@if ($errors->any())
						<div class="alert alert-danger">
							<ul>
								@foreach ($errors->all() as $error)
								<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>

						@endif
						<div class="form_grouping">
							<div class="fv-row mb-10 form_group">
								<div class="log_icons">
									<i class="fa-solid fa-user"></i>
								</div>
								<label for="email" class="form-label fs-6 fw-bolder text-dark">Email / Username</label>
								<input class="form_input form-control form-control-sm form-control-solid" type="text"
									id="email" name="email" autocomplete="off" />
							</div>
							<div class="fv-row mb-10 form_group">
								<div class="log_icons">
									<i class="fa-solid fa-lock"></i>
								</div>
								<div class="d-flex flex-stack mb-2">
									<label id="password"
										class="form-label fw-bolder text-dark fs-6 mb-0">Password</label>
								</div>
								<input class="form_input form-control form-control-sm form-control-solid"
									type="password" id="password" name="password" autocomplete="off" />
							</div>
							<div class="fv-row mb-10 fg_pw">
								<a href="{{route('forgotPassword')}}">Forgot Password?</a>
							</div>

							<div class="text-center">
								<button type="submit" class="btn btn-lg log_btn">
									<span>Login</span>
								</button>


							</div>
						</div>


					</form>
					<div class="reg_group">
						<h5 class="reg_txt">Don't have an account? </h5>
						<div class="text-center sign_btn_btn">
							<a href="{{route('userRegister')}}"><button type="button">
									<span>Register</span>
								</button></a>

						</div>
					</div>

				</div>

			</div>
		</div>
	</div>

	<!--begin::Javascript-->
	@include('admin.layouts.scripts')
	<!--end::Javascript-->
</body>
<!--end::Body-->

</html>

{{-- <script>
	 $(document).off('click', '#loginSubmit');
    $(document).on('click', '#loginSubmit', function() {
    $('#loginForm').ajaxSubmit({
        dataType: 'json'
        , success: function(response) {
            if (response.type == 'success') {
				var url=response.response;
                $.notify(response.message, 'success');
				window.location.href=url;
            } else {
				$('#error_msg').show();
				$('#error_msg_text').html(response.message);


            }
        }
    })
    })
</script> --}}
