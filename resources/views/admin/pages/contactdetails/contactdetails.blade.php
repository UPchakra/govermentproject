<div class="content_box">
	<div class="container m-12" style="margin: 0px auto !important;">
		<div class="row">
			<form id="saveContactDetailForm" method="post" class="form" enctype="multipart/form-data"
				action="{{ route('storeContactDetails') }}">
				<input type="hidden" value="{{ auth()->user()->id }}" name="userid" />
				<input type="hidden" value="{{ auth()->user()->personalid }}" name="otherdetailid" />
				<!--begin::Heading-->
				<div class="form_tab_bk">
					<div class="txt_center">
						<div class="mb-13 text-center head_text">
							<!--begin::Title-->
							<div class="head_side_design">
								<h1>सम्पर्क विवरण फारम</h1>
							</div>

							<!--end::Title-->
						</div>
					</div>
					<!--end::Heading-->
					<div class="row g2 mb-4">
						<span class="mx-0 px-0 span_custom" style="border-bottom: 1px solid; display: inline;">स्थायी
							ठेगाना
						</span>
					</div>
					<div class="row g2 mb-4">
						<!--begin::Col-->
						<div class="col-md-4  fv-row">
							<label for="provinceid" class="required fs-6 fw-bold mb-2">प्रदेश </label>
							<select class="form-select form-select-solid" id="provinceid" name="provinceid"
								style="padding: 4px;">
								<option value="">प्रदेश छान्नुहोस्</option>
								@foreach ($provinces as $province)
								<option value="{{ @$province->id }}"
									{{ $province->id==@$previousData->provinceid ? "selected" : " "}}>
									{{ @$province->provincename }}</option>
								@endforeach
							</select>

						</div>
						<!--end::Col-->
						<!--begin::Col-->
						<div class="col-md-4  fv-row">
							<label for="districtid" class="required fs-6 fw-bold mb-2">स्थायी जिल्ला </label>
							<select class="form-select districtdata form-select-solid" id="districtid" name="districtid"
								style="padding: 4px;" data-districtid="{{@$previousData->districtid}}">
								<option value="" selected>जिल्ला छान्नुहोस्</option>
							</select>
						</div>
						<!--end::Col-->
						<div class="col-md-4  fv-row">
							<label for="municipalityid" class="required fs-6 fw-bold mb-2"> नगरपालिका </label>
							<select class="form-select vdcormunicipalitydata form-select-solid" id="municipalityid"
								name="municipalityid" style="padding: 4px;"
								data-municipalityid="{{@$previousData->municipalityid}}">
								<option value="" selected>पालिकाको नाम छान्नुहोस्</option>
							</select>

						</div>



					</div>
					<div class="row g2 mb-4">

						<!--end::Col-->
						<div class="col-md-3  fv-row">

							<label for="ward" class="required fs-6 fw-bold mb-2">वार्ड नं </label>
							<input type="number" class="required form-control form_input form-control-solid"
								placeholder=" वार्ड नं   राख्नुहोस" id="ward" name="ward"
								value="{{ @$previousData->ward }}" />

						</div>
						<!--end::Col-->
						<!--begin::Col-->
						<div class="col-md-3  fv-row">
							<label for="tole" class="required fs-6 fw-bold mb-2">टोल </label>
							<input type="text" class="form-control form_input form-control-solid"
								placeholder=" टोल  राख्नुहोस" id="tole" name="tole"
								value="{{ @$previousData->tole }}" />
						</div>
						<div class="col-md-3  fv-row">
							<label for="marga" class=" fs-6 fw-bold mb-2">मार्ग </label>
							<input type="text" class="form-control form_input form-control-solid"
								placeholder=" मार्ग  राख्नुहोस" id="marga" name="marga"
								value="{{ @$previousData->marga }}" />
						</div>

						<!--end::Col-->
						<div class="col-md-3  fv-row">

							<label for="housenumber" class=" fs-6 fw-bold mb-2">
								घर नम्बर </label>
							<input type="text" class="form-control form_input form-control-solid" placeholder="
                        घर नम्बर" id="housenumber" name="housenumber" value="{{ @$previousData->housenumber }}" />

						</div>
						<!--end::Col-->
					</div>

					<div class="row g2 mb-4">
						<span class="mx-0 px-0 mt-10 span_custom" style="border-bottom: 1px solid; display: inline;">अस्थायी
							ठेगाना
						</span>
					</div>

					<div class="row g2 mb-4">
						<!--begin::Col-->
						<div class="col-md-4  fv-row">
							<label for="provinceid" class="required fs-6 fw-bold mb-2">प्रदेश </label>
							<select class="form-select form-select-solid" id="tempoprovinceid" name="tempoprovinceid"
								style="padding: 4px;">
								<option value="">प्रदेश छान्नुहोस्</option>
								@foreach ($provinces as $province)
								<option value="{{ @$province->id }}"
									{{ $province->id==@$previousData->tempoprovinceid ? "selected" : " "}}>
									{{ @$province->provincename }}</option>
								@endforeach
							</select>

						</div>
						<!--end::Col-->
						<!--begin::Col-->
						<div class="col-md-4  fv-row">
							<label for="districtid" class=" required fs-6 fw-bold mb-2">अस्थायी जिल्ला </label>
							<select class="form-select tempodistrictdata form-select-solid" id="tempodistrictid"
								name="tempodistrictid" style="padding: 4px;"
								data-tempodistrictid="{{@$previousData->tempodistrictid}}">
								<option value="" selected>जिल्ला छान्नुहोस्</option>
							</select>
						</div>
						<!--end::Col-->
						<div class="col-md-4  fv-row">
							<label for="tempomunicipalityid" class="required fs-6 fw-bold mb-2"> अस्थायी नगरपालिका </label>
							<select class="form-select tempovdcormunicipalitydata form-select-solid"
								id="tempomunicipalityid" name="tempomunicipalityid" style="padding: 4px;"
								data-municipalityid="{{@$previousData->tempomunicipalityid}}">
								<option value="" selected>पालिकाको नाम छान्नुहोस्</option>
							</select>

						</div>



					</div>
					<div class="row g2 mb-4">

						<!--end::Col-->
						<div class="col-md-3  fv-row">

							<label for="tempoward" class=" fs-6 fw-bold mb-2">वार्ड नं </label>
							<input type="number" class="form-control form_input form-control-solid"
								placeholder=" वार्ड नं   राख्नुहोस" id="tempoward" name="tempoward"
								value="{{ @$previousData->tempoward }}" />

						</div>
						<!--end::Col-->
						<!--begin::Col-->
						<div class="col-md-3  fv-row">
							<label for="tempotole" class=" fs-6 fw-bold mb-2">टोल </label>
							<input type="text" class="form-control form_input form-control-solid"
								placeholder=" टोल  राख्नुहोस" id="tempotole" name="tempotole"
								value="{{ @$previousData->tempotole }}" />
						</div>
						<div class="col-md-3  fv-row">
							<label for="tempomarga" class=" fs-6 fw-bold mb-2">मार्ग </label>
							<input type="text" class="form-control form_input form-control-solid"
								placeholder=" मार्ग  राख्नुहोस" id="tempomarga" name="tempomarga"
								value="{{ @$previousData->tempomarga }}" />
						</div>

						<!--end::Col-->
						<div class="col-md-3  fv-row">

							<label for="tempohousenumber" class=" fs-6 fw-bold mb-2">
								घर नम्बर </label>
							<input type="text" class="form-control form_input form-control-solid" placeholder="
                        घर नम्बर" id="tempohousenumber" name="tempohousenumber"
								value="{{ @$previousData->tempohousenumber }}" />

						</div>
						<!--end::Col-->
					</div>
					<div class="row g2 mb-4">


						<!--begin::Col-->
						<div class="col-md-4  fv-row">
							<label for="tempophonenumber" class=" fs-6 fw-bold mb-2">फोन नम्बर </label>
							<input type="text" class="form-control form_input form-control-solid"
								placeholder=" फोन नम्बर" id="tempophonenumber" name="tempophonenumber"
								value="{{ @$previousData->tempophonenumber }}" />
						</div>
						<div class="col-md-4  fv-row">
							<label for="mobilenumber" class=" fs-6 fw-bold mb-2">मोबाइल नम्बर </label>
							<input type="text" class="form-control form_input form-control-solid"
								placeholder=" मोबाइल नम्बर  राख्नुहोस" id="mobilenumber" name="mobilenumber"
								value="{{ @$previousData->mobilenumber }}" readonly />
						</div>
						<!--end::Col-->
						<div class="col-md-4  fv-row">

							<label for="email" class=" fs-6 fw-bold mb-2">Email</label>
							<input type="text" class="form-control form_input form-control-solid" placeholder="Email"
								id="email" name="email" value="{{ @$previousData->email }}" readonly />

						</div>
						<!--end::Col-->
					</div>
					<div class="row g2 mb-4">


						<div class="col-md-6  fv-row">

							<label for="maillingaddress" class=" fs-6 fw-bold mb-2">पत्राचार ठेगाना</label>
							<input type="text" class="form-control form_input form-control-solid"
								placeholder="पत्राचार ठेगाना" id="maillingaddress" name="maillingaddress"
								value="{{ @$previousData->maillingaddress }}" />

						</div>

					</div>
					<div class="row">

					</div>
				</div>



			</form>
		</div>
			<div class="row">
				<div class="col-md-12">
				<div class="btn_flx">
					<button class="btn btn-primary prev_btn" type="button"
						onclick="navigaterTo('otherdetailForm')"
						style="margin-top: 0px; padding: 8px 30px;">Previous</button>
					<button class="btn btn-primary mr-12" type="button" id="saveContactDetail">Next</button>
			</div>
				</div>
		</div>


	</div>
</div>
<script>
$(document).ready(function() {
	$(document).off('click', '#saveContactDetail');
	$(document).on('click', '#saveContactDetail', function() {

		$('#saveContactDetailForm').ajaxSubmit({
			dataType: 'json',
			success: function(response) {
				responseData = response[0];
				if (responseData.success) {
					navigaterTo(responseData.redirectUrl);
					$.notify(responseData.message, 'success');
				} else {
					$.notify(responseData.message, 'error');
				}
			}
		});

		$('#provinceid').trigger('click');
		$('#districtid').trigger('click');
		$('#tempoprovinceid').trigger('click');
		$('#tempodistrictid').trigger('click');
	})
})
</script>


<script>
$(document).ready(function() {
	$(document).off('click', '#provinceid');
	$(document).on('change', '#provinceid', function() {

		var provinceid = $(this).find('option:selected').val();
		var districtid = $('#districtid').data('districtid');

		var url = baseUrl + '/provincewisedistrict';
		var infoData = {
			'provinceid': provinceid,
			'districtid': districtid,

		};

		$.post(url, infoData, function(response) {
			$('.districtdata').html(response);
		})
	});

	$(document).off('click', '#districtid');
	$(document).on('change', '#districtid', function() {

		var districtid = $(this).find('option:selected').val();
		var municipalityid = $('#municipalityid').data('municipalityid');
		var did = $('#districtid').data('districtid');


		var url = baseUrl + '/districtwisevdcormunicipality';
		var infoData = {
			'districtid': districtid,
			'municipalityid': municipalityid,
			'did': did
		};

		$.post(url, infoData, function(response) {
			$('.vdcormunicipalitydata').html(response);

		})
	});


})


$(document).ready(function() {
	$(document).off('click', '#tempoprovinceid');
	$(document).on('change', '#tempoprovinceid', function() {

		var provinceid = $(this).find('option:selected').val();
		var districtid = $('#tempodistrictid').data('districtid');

		var url = baseUrl + '/provincewisedistrict';
		var infoData = {
			'provinceid': provinceid,
			'districtid': districtid,

		};

		$.post(url, infoData, function(response) {
			$('.tempodistrictdata').html(response);
		})
	});

	$(document).off('click', '#tempodistrictid');
	$(document).on('change', '#tempodistrictid', function() {

		var districtid = $(this).find('option:selected').val();
		var municipalityid = $('#tempomunicipalityid').data('municipalityid');
		var did = $('#tempodistrictid').data('tempodistrictid');


		var url = baseUrl + '/districtwisevdcormunicipality';
		var infoData = {
			'districtid': districtid,
			'municipalityid': municipalityid,
			'did': did
		};

		$.post(url, infoData, function(response) {
			$('.tempovdcormunicipalitydata').html(response);

		})
	});


})
</script>