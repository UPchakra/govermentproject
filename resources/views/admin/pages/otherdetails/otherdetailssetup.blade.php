<div class="content_box">
    <div class="container my-12">
        <div class="row">
            <div class="col-12">
                <form id="extraDetailsForm" method="post" class="form" enctype="multipart/form-data"
                    action="{{ @$saveurl }}">
                    <input type="hidden" value="{{ auth()->user()->id }}" name="userid" />
                    <input type="hidden" value="{{ @$previousData->id }}" name="extradetailid" />
                    <input type="hidden" value="{{ auth()->user()->personalid }}" name="personaldetailid" />
                    <div class="form_tab_bk">
                        <!--begin::Heading-->
                        <div class="txt_center">
                            <div class="mb-13 text-center head_text">
                                <!--begin::Title-->
                                <div class="head_side_design">
                                    <h1>अन्य व्यक्तिगत विवरण फारम</h1>
                                </div>

                                <!--end::Title-->
                            </div>
                        </div>
                        <!--end::Heading-->
                        <div class="row g2 mb-4">
                            <!--begin::Col-->

                            <div class="col-md-3 fv-row">
                                <label for="cast" class="required fs-6 fw-bold mb-2">जात</label>
                                <select class="form-select  select2 form-select-solid" id="cast" name="cast"
                                    style="padding: 4px;">
                                    <option value="" selected>जात छान्नुहोस्</option>
                                    <option value="Brahmin/Chhetrri"
                                        {{ @$previousData->cast == 'Brahmin/Chhetrri' ? 'selected' : '' }}>
                                        Brahmin/Chhetrri</option>
                                    <option value="Janajaati"
                                        {{ @$previousData->cast == 'Janajaati' ? 'selected' : '' }}>Janajaati
                                    </option>
                                    <option value="Dalit" {{ @$previousData->cast == 'Dalit' ? 'selected' : '' }}>
                                        Dalit
                                    </option>
                                    <option value="Others" {{ @$previousData->cast == 'Others' ? 'selected' : '' }}>
                                        Others
                                    </option>
                                </select>

                            </div>
                            <!--end::Col-->
                            <!--end::Col-->
                            <div class="col-md-3 fv-row">
                                <label for="casteother" class="fs-6 fw-bold mb-2">अन्य जात</label>
                                <input type="text" class="form-control form_input form-control-solid"
                                    placeholder="अन्य जात" id="casteother" name="casteother"
                                    value="{{ @$previousData->casteother }}" />

                            </div>
                            <!--end::Col-->
                            <div class="col-md-3 fv-row">
                                <label for="religion" class="required fs-6 fw-bold mb-2">धर्म</label>
                                <select type="select" class="form-control form_input form-control-solid" id="religion"
                                    name="religion">
                                    <option value="" selected>धर्म छान्नुहोस्</option>

                                    <option value="Hindu"
                                        {{ @$previousData->religion == 'Hindu' ? 'selected' : '' }}>
                                        Hindu</option>
                                    <option value="Muslim"
                                        {{ @$previousData->religion == 'Muslim' ? 'selected' : '' }}>
                                        Muslim</option>
                                    <option value="christian"
                                        {{ @$previousData->religion == 'christian' ? 'selected' : '' }}>christian
                                    </option>
                                    <option value="Others"
                                        {{ @$previousData->religion == 'Others' ? 'selected' : '' }}>
                                        Others</option>

                                </select>
                            </div>
                            <!--end::Col-->
                            <div class="col-md-3 fv-row">
                                <label for="religionother" class=" fs-6 fw-bold mb-2">अन्य धर्म</label>
                                <input type="text" class="form-control form_input form-control-solid"
                                    placeholder="अन्य धर्म" id="religionother" name="religionother"
                                    value="{{ @$previousData->religionother }}" />

                            </div>
                            <!--end::Col-->
                        </div>
                        <!--end::Input group-->
                        <div class="row g2 mb-4">
                            <!--begin::Col-->

                            <div class="col-md-4 fv-row">
                                <label for="maritalstatus" class="required fs-6 fw-bold mb-2">वैवाहिक स्थिति</label>
                                <select type="select" class="form-control form_input form-control-solid"
                                    placeholder="वैवाहिक स्थिति" id="maritalstatus" name="maritalstatus">
                                    <option value="" selected>रोजगारी अवस्था छान्नुहोस्</option>
                                    <option value="Married"
                                        {{ @$previousData->maritalstatus == 'Married' ? 'selected' : '' }}>Married
                                    </option>
                                    <option value="Single"
                                        {{ @$previousData->maritalstatus == 'Single' ? 'selected' : '' }}>Single
                                    </option>
                                    <option value="Divorcee"
                                        {{ @$previousData->maritalstatus == 'Divorcee' ? 'selected' : '' }}>Divorcee
                                    </option>
                                </select>

                            </div>
                            <!--end::Col-->
                            <div class="col-md-4 fv-row">
                                <label for="employmentstatus" class="required fs-6 fw-bold mb-2">रोजगारी अवस्था </label>
                                <select type="select" class="form-control form_input form-control-solid"
                                    placeholder="रोजगारी अवस्था " id="employmentstatus" name="employmentstatus">
                                    <option value="" selected>रोजगारी अवस्था छान्नुहोस्</option>
                                    <option value="Employeed"
                                        {{ @$previousData->employmentstatus == 'Employeed' ? 'selected' : '' }}>
                                        Employeed</option>
                                    <option value="Unemployeed"
                                        {{ @$previousData->employmentstatus == 'Unemployeed' ? 'selected' : '' }}>
                                        Unemployeed</option>
                                    <option value="Others"
                                        {{ @$previousData->employmentstatus == 'Others' ? 'selected' : '' }}>Others
                                    </option>

                                </select>

                            </div>
                            <!--end::Col-->
                            <div class="col-md-4 fv-row">
                                <label for="employmetothers" class="fs-6 fw-bold mb-2">अन्य रोजगारी </label>
                                <input type="text" class="form-control form_input form-control-solid"
                                    placeholder="अन्य रोजगारी " id="employmetothers" name="employmetothers"
                                    value="{{ @$previousData->employmetothers }}" />

                            </div>
                            <!--end::Col-->
                        </div>
                        <!--end::Input group-->
                        <!--begin::Input group-->
                        <div class="row g2 mb-4">

                            <!--begin::Col-->
                            <div class="col-md-4 fv-row">
                                <label for="motherlanguage" class="required fs-6 fw-bold mb-2">मातृभाषा</label>
                                <input type="text" class="form-control form_input form-control-solid"
                                    placeholder="मातृभाषा राख्नुहोस" id="motherlanguage" name="motherlanguage"
                                    value="{{ @$previousData->motherlanguage }}" />

                            </div>
                            <!--end::Col-->
                            <!--begin::Col-->
                            <div class="col-md-4 fv-row">
                                <label for="disabilitystatus" class="required fs-6 fw-bold mb-2">शारीरिक रुपमा आशक्त
                                    ?</label>
                                <select type="text" class="form-control form_input form-control-solid"
                                    placeholder="शारीरिक रुपमा आशक्त ?" id="disabilitystatus" name="disabilitystatus"
                                    value="{{ @$previousData->disabilitystatus }}">
                                    <option value="Yes" selected>Yes</option>
                                    <option value="No">No</option>
                                </select>


                            </div>
                            <!--end::Col-->
                            <!--begin::Col-->
                            <div class="col-md-4 fv-row">
                                <label for="disabilityoverview" class="required fs-6 fw-bold mb-2">शारीरिक आशक्त खुला
                                    उनुहोस</label>
                                <input type="text" class="form-control form_input form-control-solid"
                                    placeholder="शारीरिक आशक्त खुलाउनुहोस" id="disabilityoverview"
                                    name="disabilityoverview" value="{{ @$previousData->disabilityoverview }}" />

                            </div>
                            <!--end::Col-->

                        </div>


                    </div>


                </form>

            </div>
            <div class="col-md-12 ">
                <div class="btn_flx">
                    <button class="btn_prev" type="button" onclick="navigaterTo('personal')">Previous</button>
                    <button class="btn_save" type="button" id="saveOtherDetail">Next</button>

                </div>
            </div>


        </div>

    </div>
</div>
<script>
    $(document).ready(function() {
        $(document).off('click', '#saveOtherDetail');
        $(document).on('click', '#saveOtherDetail', function() {
            $('#extraDetailsForm').ajaxSubmit({
                dataType: 'json',
                success: function(response) {
                    responseData = response[0];
                    if (responseData.success) {
                        navigaterTo(responseData.redirectUrl);
                        $.notify(responseData.message, 'success');
                    } else {
                        $.notify(responseData.message, 'error');
                    }
                }
            });
        })
    })
</script>


<script>
    $(document).ready(function() {
        $(document).off('click', '#provinceid');
        $(document).on('click', '#provinceid', function() {

            var provinceid = $(this).find('option:selected').val();
            var districtid = $('#districtid').data('districtid');

            var url = baseUrl + '/provincewisedistrict';
            var infoData = {
                'provinceid': provinceid,
                'districtid': districtid,

            };

            $.post(url, infoData, function(response) {
                $('.districtdata').html(response);
            })
        });

        $(document).off('click', '#mailingprovinceid');
        $(document).on('click', '#mailingprovinceid', function() {

            var provinceid = $(this).find('option:selected').val();
            var districtid = $('#mailingdistrictid').data('mailingdistrictid');

            var url = baseUrl + '/provincewisedistrict';
            var infoData = {
                'provinceid': provinceid,
                'districtid': districtid,

            };

            $.post(url, infoData, function(response) {
                $('.mailingdistrictdata').html(response);
            })
        });


    })

    $(document).ready(function() {
        $(document).off('click', '#districtid');
        $(document).on('click', '#districtid', function() {

            var districtid = $(this).find('option:selected').val();
            var municipalityid = $('#municipalityid').data('municipalityid');
            var did = $('#districtid').data('districtid');


            var url = baseUrl + '/districtwisevdcormunicipality';
            var infoData = {
                'districtid': districtid,
                'municipalityid': municipalityid,
                'did': did
            };

            $.post(url, infoData, function(response) {
                $('.vdcormunicipalitydata').html(response);

            })
        });
        $(document).off('click', '#mailingdistrictid');
        $(document).on('click', '#mailingdistrictid', function() {

            var districtid = $(this).find('option:selected').val();
            var municipalityid = $('#mailingdistrictid').data('mailingdistrictid');
            var did = $('#mailingdistrictid').data('mailingdistrictid');


            var url = baseUrl + '/districtwisevdcormunicipality';
            var infoData = {
                'districtid': districtid,
                'municipalityid': municipalityid,
                'did': did
            };

            $.post(url, infoData, function(response) {
                $('.mailingvdcormunicipalitydata').html(response);

            })
        });


    })
</script>
