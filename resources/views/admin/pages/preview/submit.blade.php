<div class="post d-flex flex-column-fluid" id="kt_post">
    <!--begin::Container-->
    <div id="kt_content_container" class="container-xxl">
        <!--begin::Card-->
        <div class="card">
            <!--begin::Card header-->
            <div class="card-header border-0 pt-6">
                <!--begin::Card toolbar-->
                <div class="card-toolbar">
                    <h3 style="font-weight: 600; color: #08c; border-bottom: 5px solid #08c;">उपलब्ध विज्ञापन</h3>
                </div>
                <!--end::Card toolbar-->
            </div>
            <!--end::Card header-->
            <!--begin::Card body-->
            <div class="card-body py-4">
                <div class="row p-0 m-0">
                    <div class="col-12">
                        <table class="w-100" style="border:1px solid black" id="availableJobs">
                            <tr>
                                <th>SN</th>
                                <th>पद - सेवा÷समूह </th>
                                <th>विज्ञापन नं</th>
                                <th>खुला र समावेशी </th>
                                <th>Action</th>
                            </tr>
                            @foreach (@$vacancy as $key => $vacantpost)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $key }}</td>
                                    <td>{{ implode(", ",$vacantpost['vacancycode']); }}</td>
                                    <td>
                                        {{-- {{ dd($vacantpost['jobcat']) }} --}}
                                        @foreach ($vacantpost['jobcat'] as $keys => $vals)
                                            {{ $keys }}
                                            @php
                                                $numberofvacancy = 0;
                                            @endphp
                                            @foreach ($vals as $val)
                                                @php
                                                    $numberofvacancy += $val->numberofvacancy;
                                                @endphp
                                            @endforeach
                                            -{{$numberofvacancy}}

                                             ,
                                        @endforeach
                                    </td>
                                    <td>
                                        <button type="button" class="btn-sm btn-primary "
                                            onclick="advertPop({{ $vacantpost['designation'] }},{{ $vacantpost['servicesgroup'] }})">
                                            Apply
                                        </button>
                                    </td>
                                </tr>
                            @endforeach
                        </table>

                    </div>
                </div>
            </div>
            <!--end::Card body-->
        </div>
        <!--end::Card-->
    </div>
    <!--end::Container-->
</div>
<div class="modal fade" id="vacancydetailModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
    aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle"></h5>
                <button type="button" id="closedmodal" class="close  pull-right" value="vacancydetailModal"
                    data-dismiss="modal">&times;</button>
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="vacancydetails">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>
<style>
    th {
        background: #f7f7f7;
        color: black;
        border: 1px solid #ddd;
        text-align: center;
        padding: 8px;
    }
    td{
        border: 1px solid #ddd;
        padding: 8px;
        text-align: center;
    }
    .cl_btn{
        border: none;
        padding: 6px 25px;
        font-size: 14px;
        font-weight: 600;
    }
</style>
<script>
    function advertPop(designationid, servicesgroupid) {
        $("#vacancydetails").html()
        var data = {
            designationid: designationid,
            servicesgroupid: servicesgroupid,
        };
        var url = "{{ route('getjobdetails') }}";
        $.ajax({
            type: 'POST',
            url: url,
            data: data,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(response) {
                // $("#vacancydetails").html(response);
                // jQuery.noConflict();
                // $('#vacancydetailModal').modal('show');
                $("#vacancydetails").html(response)
                $("#vacancydetailModal").modal('show');
            }
        });

    }
</script>
