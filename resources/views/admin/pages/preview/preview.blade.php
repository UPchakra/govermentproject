<head>
    <style>
        .row_custom{
            padding: 0px 15px;
    margin-bottom: 15px !important;
        }
        .row_custom .col-md-12{
            padding: 15px;
    box-shadow: 0px 0px 5px rgb(0 0 0 / 20%);
        }
        table{
            border: none;
            
        }
        .title_add{
            padding: 0px 15px;
            margin-top: 15px;
            margin-bottom: 8px;
            margin-bottom: 8px;
        }
        .title_add span{
        border-bottom: 1px solid #ddd; color: #08c; font-size: 18px; font-weight: 600; display: inline;
        }
        table td span{
            font-weight: 600;
    padding-left: 15px;
        }
    </style>
</head>


<div class="post d-flex flex-column-fluid" id="kt_post">
    <!--begin::Container-->
    <div id="kt_content_container" class="container-xxl">
        <!--begin::Card-->
        <div class="container" style="border: 1px solid #ddd; border-radius: 5px;">
            {{-- Personal details --}}
            <div class="row g2 mb-2 title_add">
                <span class="mx-0 px-0" style="">ब्यतिगत विवारण </span>
            </div>
            <div class="row g2 mb-2 row_custom" style="width: 20%;  margin: 0px auto;">
                <div class="col-md-12" style="box-shadow: 0px 0px 4px rgba(0,0,0,0.2); display: flex; justify-content: center;">
                    <img src="{{ @$user->documents->photography ?? "https://picsum.photos/200/300" }}" style="width: 100px;"
                        alt="">
                </div>

            </div>
            <div class="row g2 mb-2 row_custom">
                <div class="col-md-12">
                    <table class="w-100">
                        <tr>
                            <td><strong> नाम देवनगरिमा :</strong><span>{{ @$profile->nfirstname . ' ' . @$profile->nmiddlename . ' ' . @$profile->nlastname ?? "नाम देवनगरिमा"}}</span></td>
                            <td></td>
                            <td><strong> नाम अग्रेजि :</strong><span>{{ @$profile->efirstname . ' ' . @$profile->emiddlename . ' ' . @$profile->elastname ?? "नाम अग्रेजि"  }}</span></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td><strong> Gender :</strong><span>{{ @$profile->gender ?? "Gender" }}</span></td>
                            <td></td>
                            <td><strong> Date Of Birth :</strong><span>{{ @$profile->dateofbirthbs ?? "Date of Birth" }}</span></td>
                            <td></td>
                        </tr>
                    </table>
                </div>

            </div>
            <div class="row g2 mb-2 row_custom">
                <div class="col-md-12">
                    <table class="w-100">
                        <tr>
                            <td><strong> नागरिकता नं :</strong><span>{{ @$profile->citizenshipnumber}}</span></td>
                            <td></td>
                            <td><strong> जारी जिल्ला :</strong><span>-Text-</span></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td><strong> नागरिकता जारी मिती :</strong><span>{{ @$profile->citizenshipissuedate }}</span></td>
                            <td></td>
                        </tr>
                    </table>
                </div>

            </div>
            <div class="row g2 mb-2 row_custom">
                <div class="col-md-12">
                    <table class="w-100">
                        <tr>
                            <td><strong> मातृभाषा :</strong><span>{{ @$extraDetails->motherlanguage }}</span></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td><strong> शारीरिक रुपमा आशक्त ?:</strong><span>{{ @$extraDetails->disabilitystatus}}</span></td>
                            <td></td>
                        </tr>
                    </table>
                </div>

            </div>
            
            <div class="row g2 mb-2 row_custom">
                <div class="col-md-12">
                    <table class="w-100">
                        <tr>
                            <td><strong> आमा को नाम :</strong><span>{{ @$profile->motherfirstname . ' ' . @$profile->mothermiddlename . ' ' . @$profile->motherlastname ?? "mother" }}</span></td>
                            <td></td>

                        </tr>
                        <tr>
                            <td><strong> बाबु को नाम :</strong><span>{{ @$profile->fatherfirstname . ' ' . @$profile->fathermiddlename . ' ' . @$profile->fatherlastname ??  "father" }}</span></td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td><strong> बाजेको नाम :</strong><span>{{ @$profile->grandfatherfirstname . ' ' . @$profile->grandfathermiddlename . ' ' . @$profile->grandfatherlastname ?? "बाजेको" }}</span></td>
                            <td>
                            </td>
                        </tr>
                    </table>
                </div>

            </div>
            {{-- Personal  details End --}}

            <div class="row g2 mb-2 title_add">
                <span class="mx-0 px-0" style="border-bottom: 1px solid; display: inline;">स्थाई ठेगाना</span>
            </div>
            <div class="row g2 mb-2 row_custom">
                <div class="col-md-12">
                    <table class="w-100">
                        <tr>
                            <td><strong> प्रदेश :</strong><span>{{ @$contactDetails->provincename }}</span></td>
                            <td></td>
                            <td><strong> स्थायी जिल्ला:</strong><span>{{ @$contactDetails->districtname }}</span></td>
                            <td></td>
                            <td><strong> नगरपालिका:</strong><span>{{ @$contactDetails->vdcormunicipalitiename }}</span></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td><strong> वार्ड नं :</strong><span>{{ @$contactDetails->ward }}</span></td>
                            <td></td>
                            <td><strong> टोल:</strong><span>{{ @$contactDetails->tole }}</span></td>
                            <td></td>
                            <td><strong> घर नम्बर:</strong><span>{{ @$contactDetails->housenumber }}</span></td>
                            <td></td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="row g2 mb-2 row_custom">
                <div class="col-md-12">
                    <table class="w-100">
                        <tr>
                            <td><strong> फोन नम्बर :</strong><span>{{ @$contactDetails->phonenumber }}</span></td>
                            <td></td>
                            <td><strong> मोबाइल नम्बर :</strong><span>{{ @$contactDetails->mobilenumber }}</span></td>
                            <td></td>
                            <td><strong> Email :</strong><span>{{ @$contactDetails->email }}</span></td>
                            <td></td>
                        </tr>
                    </table>
                </div>
            </div>

            <div class="row g2 mb-2 title_add">
                <span class="mx-0 px-0" style="border-bottom: 1px solid; display: inline;">अस्थायी ठेगाना</span>
            </div>
            <div class="row g2 mb-2 row_custom">
                <div class="col-md-12">
                    <table class="w-100">
                        <tr>
                            <td><strong> प्रदेश :</strong><span>{{ @$contactDetails->tempprovincename }}</span></td>
                            <td></td>
                            <td><strong> स्थायी जिल्ला:</strong><span>{{ @$contactDetails->tempdistrictname }}</span></td>
                            <td></td>
                            <td><strong> नगरपालिका:</strong><span>{{ @$contactDetails->tempvdcormunicipalitiename }}</span></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td><strong> वार्ड नं :</strong><span>{{ @$contactDetails->tempoward }}</span></td>
                            <td></td>
                            <td><strong> टोल:</strong><span>{{ @$contactDetails->tempotole }}</span></td>
                            <td></td>
                            <td><strong> घर नम्बर:</strong><span>{{ @$contactDetails->tempohousenumber }}</span></td>
                            <td></td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="row g2 mb-2 row_custom">
                <div class="col-md-12">
                    <table class="w-100">
                        <tr>
                            <td><strong> फोन नम्बर :</strong><span>{{ @$contactDetails->tempophonenumber }}</span></td>
                            <td></td>
                            <td><strong> मोबाइल नम्बर :</strong><span>{{ @$contactDetails->mobilenumber }}</span></td>
                            <td></td>
                            <td><strong> Email :</strong><span>{{ @$contactDetails->email }}</span></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td><strong> पत्राचार ठेगाना :</strong><span>{{ @$contactDetails->maillingaddress }}</span></td>
                            <td></td>
                        </tr>
                    </table>
                </div>
            </div>

            {{-- Educational details --}}
            <div class="row g2 mb-2 title_add">
                <span class="mx-0 px-0" style="border-bottom: 1px solid; display: inline;">शैक्षिक विवरण</span>
            </div>
            <div class="row g2 mb-2 row_custom">
                <div class="col-md-12">
                    <table class="w-100">
                        <tr style="background: skyblue">
                           <th>कलेज/विद्यालय/संस्थाको नाम</th>
                           <th>विश्वविद्यालय / बोर्ड नाम</th>
                           <th>उतिर्ण गरेको परिक्षा/ स्तर</th>
                           <th>संकाय</th>
                           <th>श्रेणि</th>
                           <th>कुल अंक/प्रतिशत</th>
                           <th>प्रमुख विषयहरू</th>
                        </tr>
                        @foreach(@$educationDetails as $eduval)
                        <tr>
                           <td>{{$eduval->educationinstitution}}</td>
                           <td>{{$eduval->universityboardname}}</td>
                           <td>{{$eduval->name}}</td>
                           <td>{{$eduval->educationfaculty}}</td>
                           <td>--Text श्रेणि-</td>
                           <td>{{$eduval->devisiongradepercentage}}</td>
                           <td>{{$eduval->mejorsubject}}</td>
                        </tr>
                        @endforeach
                    </table>
                </div>

            </div>
            {{-- Educational details end --}}

            {{-- Training details --}}
            <div class="row g2 mb-2 title_add">
                <span class="mx-0 px-0" style="border-bottom: 1px solid; display: inline;">तालिम विवरण</span>
            </div>
            <div class="row g2 mb-2 row_custom">
                <div class="col-md-12">
                    <table class="w-100">
                        <tr style="background: skyblue">
                           <th>तालिम प्रदायकको नाम</th>
                           <th>तालिमको विषय</th>
                           <th>ग्रेड/प्रतिशत </th>
                           <th>अवधि देखि </th>
                            <th>अवधि सम्म </th>
                        </tr>
                        @foreach($trainingDetails as $traval)
                        <tr>
                           <td>{{$traval->trainingproviderinstitutionalname}}</td>
                           <td>{{$traval->trainingname}}</td>
                           <td>{{$traval->gradedivisionpercent}}</td>
                           <td>{{$traval->fromdatebs}}</td>
                           <td>{{$traval->enddatebs}}</td>
                        </tr>
                        @endforeach
                    </table>
                </div>

            </div>
            {{-- Training details end --}}

            {{-- Experience details --}}
            <div class="row g2 mb-2 title_add">
                <span class="mx-0 px-0" style="border-bottom: 1px solid; display: inline;">अनुभव विवरण</span>
            </div>
            <div class="row g2 mb-2 row_custom">
                <div class="col-md-12">
                    <table class="w-100">
                        <tr style="background: skyblue">
                           <th>कार्यालय</th>
                           <th>कार्यालय ठेगाना</th>
                           <th>पद</th>
                           <th>तह</th>
                           <th>सेवा/समूह </th>
                           <th>सुरू मिति  </th>
                           <th>अन्त्य मिति</th>
                           <th>कार्य विवरण</th>
                        </tr>
                        @foreach($experienceDetails as $expval)
                        <tr>
                           <td>{{$expval->officename}}</td>
                           <td>{{$expval->officeaddress}}</td>
                           <td>{{$expval->designation}}</td>
                           <td>{{$expval->ranklabel}}</td>
                           <td>{{$expval->service}}/{{$expval->group}}</td>
                           <td>{{$expval->fromdatebs}}</td>
                           <td>{{$expval->enddatebs}}</td>
                           <td>{{$expval->workingstatus}}</td>
                        </tr>
                        @endforeach
                    </table>
                </div>

            </div>
            {{-- Experience details end --}}

        </div>

        <!--end::Card-->
    </div>
    <!--end::Container-->
</div>
</div>
