@component('mail::message')
# Password Recovery
<p></p><br>
<p>Please use this password to login.<br>
<h3>{{ $password }}</h3><br>
Don't forget to update password once you logged in.</p>
@component('mail::button', ['url' => url('/adminlogin')])
Login
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
