@component('mail::message')
# Verify Your Email
<p>Hi {{$email}}</p><br>
<p>Please verify using this OTP.<br>
{{ $otp }}
</p>

Thanks,<br>
{{ config('app.name') }}
@endcomponent
